//
//  DrawerView.swift
//  NavigationDrawer
//
//  Created by Sowrirajan Sugumaran on 05/10/17.
//  Copyright © 2017 Sowrirajan Sugumaran. All rights reserved.
//

import UIKit

// Delegate protocolo for parsing viewcontroller to push the selected viewcontroller
protocol DrawerControllerDelegate: class {
    func pushTo(viewController : String)
}

class DrawerView: UIView, drawerProtocolNew, UIGestureRecognizerDelegate {
    
    
    lazy var navigationBar: NavView = {
        let header = NavView()
        header.menuBtn.setImage(UIImage(named: "arrow-back"), for: .normal)
        header.titleLbl.isHidden = true
        header.contentView.backgroundColor = .clear
        header.msgSettingBtn.isHidden = true
        header.menuBtn.addTarget(self, action: #selector(actDissmiss), for: .touchUpInside)
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    var viewModel = SideMenuViewModel()
    
    
    var aryViewControllers = [String]()
    
    weak var delegate: DrawerControllerDelegate?
    
    public private(set) lazy var screenEdgePanGesture: UIScreenEdgePanGestureRecognizer = {
        let gesture = UIScreenEdgePanGestureRecognizer(
            target: self,
            action: #selector(DrawerView.handlePanGesture(_:))
        )
        gesture.delegate = self
        return gesture
    }()
    
    public private(set) lazy var panGesture: UIPanGestureRecognizer = {
        let gesture = UIPanGestureRecognizer(
            target: self,
            action: #selector(DrawerView.handlePanGesture(_:))
        )
        gesture.delegate = self
        return gesture
    }()
    
    public private(set) lazy var tapGesture: UITapGestureRecognizer = {
        let gesture = UITapGestureRecognizer(
            target: self,
            action: #selector(DrawerView.handlePanGesture(_:))
        )
        gesture.delegate = self
        return gesture
    }()
    
    var backgroundView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.alpha = 0.7
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var drawerView:UIView={
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.alpha = 0.9
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    convenience init(aryControllers: [String], controller:UIViewController) {
        self.init(frame: UIScreen.main.bounds)
        self.tableView.register(SideMenuTableViewCell.nib, forCellReuseIdentifier: SideMenuTableViewCell.identifier)
        self.initialise(controllers: aryControllers, controller:controller)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialise(controllers:[String], controller:UIViewController) {
        aryViewControllers = controllers
        setupViews()
    }
    
    private func setupViews() {
        backgroundView.addGestureRecognizer(tapGesture)
        backgroundView.addGestureRecognizer(screenEdgePanGesture)
        backgroundView.addGestureRecognizer(panGesture)
        addSubview(backgroundView)
        addSubview(drawerView)
        drawerView.addSubview(navigationBar)
        drawerView.addSubview(tableView)
        setupConstraints()
    }
    
    private func setupConstraints() {
        backgroundView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        backgroundView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        backgroundView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        backgroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        drawerView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        drawerView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        drawerView.widthAnchor.constraint(equalToConstant: screenSize.width*0.8).isActive = true
        drawerView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        var topConst: CGFloat = 20
        var navHeight: CGFloat = 44
        if hasTopNotch {
            topConst = 0
            navHeight *= 2
        }
        navigationBar.topAnchor.constraint(equalTo: drawerView.topAnchor, constant:topConst).isActive = true
        navigationBar.leftAnchor.constraint(equalTo:  drawerView.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo:  drawerView.rightAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: navHeight).isActive = true
        
        tableView.topAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant:12).isActive = true
        tableView.leftAnchor.constraint(equalTo: drawerView.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: drawerView.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: drawerView.bottomAnchor).isActive = true
    }
    

    // To dissmiss the current view controller tab bar along with navigation drawer
    @objc func actDissmiss() {
        self.dissmiss()
    }
    
    
    @objc final func handlePanGesture(_ sender: UIGestureRecognizer) {
       
        switch sender.state {
        case .ended, .cancelled:
            self.actDissmiss()
        default:
            break
        }
    }
}

extension DrawerView: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.menus.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SideMenuTableViewCell.identifier) as! SideMenuTableViewCell
        cell.backgroundColor = UIColor.clear
        cell.model = viewModel.menus[indexPath.row]
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        actDissmiss()
        //Logger.log(message: "row selected: \(viewModel.menus[indexPath.row].title)", event: .i)
        //        let storyBoard = UIStoryboard(name:"Main", bundle:nil)
        //        let controllerName = (storyBoard.instantiateViewController(withIdentifier: aryViewControllers[indexPath.row] as! String))
        self.delegate?.pushTo(viewController: aryViewControllers[indexPath.row])
    }
    
}
