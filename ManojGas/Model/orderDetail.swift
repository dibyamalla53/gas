//
//  orderDetail.swift
//  ManojGas
//
//  Created by Mac on 6/14/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import Foundation

struct Details{
    var orderCode: String?
    var customerName: String?
    var customerAddress: String?
    var contactNo: String?
    var requestedDate: String?
    var respondedDate: String?
    var confirmedDate: String?
    var deliveredDate: String?
}
