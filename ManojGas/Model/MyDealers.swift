//
//  MyDealers.swift
//  ManojGas
//
//  Created by Mac on 4/29/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import Foundation

struct Dealers {
    var dealerName: String?
    var address: String?
    var mobNumber: String?
    var uuid: String?
    var savedDealers: String?
}
