//
//  dealerList.swift
//  ManojGas
//
//  Created by Mac on 5/23/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import Foundation

struct List {
    var gasAvailable: String?
    var name: String?
    var address: String?
    var contact: String?
    var uuid: String?
    var saved: String?
    
}
