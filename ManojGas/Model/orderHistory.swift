//
//  orderHistory.swift
//  ManojGas
//
//  Created by Mac on 4/26/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import Foundation

struct HistoryInfo
{
    var dealerName:String?
    var dealerAddress:String?
    var createdAt:String?
    var quantity:Int?
    var active: String?
    var orderId: String?
    var orderCode: String?
    var orderStatus: String?
    var orderSent: String?
}
