//
//  location.swift
//  ManojGas
//
//  Created by Mac on 6/26/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import Foundation

struct locations {
    var uuid: String?
    var name: String?
    var latitude: Double?
    var longitude: Double?
}
