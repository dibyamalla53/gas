//
//  NavView.swift
//  ManojGas
//
//  Created by f1soft on 11/12/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import UIKit

class NavView: UIView, UINavigationControllerDelegate  {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var navImgView: UIImageView!
    @IBOutlet weak var navBtnLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var centerYConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var msgSettingBtn: UIButton!
    @IBAction func msgSettingBtn(_ sender: UIButton) {
        
    }
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    private func setupViews() {
        UINib(nibName: "NavView", bundle: nil).instantiate(withOwner: self, options: nil)
        addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        contentView.widthAnchor.constraint(equalToConstant: screenSize.width).isActive = true
        if hasTopNotch {
            centerYConstraint.constant = -4
            navBtnLeadingConstraint.constant = 20
            navImgView.image = UIImage(named: "top-bg-x")
        }
        
    }

}
