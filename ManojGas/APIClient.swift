//
//  APIClient.swift
//  Mero Surakshya
//
//  Created by f1soft on 6/21/18.
//  Copyright © 2018 Shiran. All rights reserved.
//

import Foundation

class APIClient {
    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
}

extension APIClient{
    typealias JSONTaskCompletionHandler = (Decodable?, APIError?) -> Void
   private func decodingTask<T: Decodable>(with request: URLRequest, decodingType: T.Type, completionHandler completion: @escaping JSONTaskCompletionHandler) -> URLSessionDataTask? {
         dataTask = defaultSession.dataTask(with: request) { data, response, error in
            Logger.log(message: "error: \(String(describing: error))", event: .i)
            if let error = error as NSError?, error.domain == NSURLErrorDomain && error.code == NSURLErrorCancelled {
                 return
            }
            else if let error = error as? URLError, error.code  == URLError.Code.notConnectedToInternet
            {
                completion(nil, .noInternet)
                return
            }
           
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(nil, .requestFailed)
                return
            }
            if httpResponse.statusCode == 200 {
                if let data = data {
                    do {
                    
                        let genericModel = try JSONDecoder().decode(decodingType, from: data)
//                        print("genericModel: \(genericModel), json: \(json)")
                        completion(genericModel, nil)
                    } catch let error {
                        Logger.log(message: "error: \(error)", event: .i)
                        completion(nil, .jsonConversionFailure)
                    }
                } else {
                    completion(nil, .invalidData)
                }
            } else {
                completion(nil, .responseUnsuccessful)
            }
        }
        return dataTask
    }
    
    
    func fetch<T: Decodable>(with request: URLRequest, decode: @escaping (Decodable) -> T?, completion: @escaping (Result<T, APIError>) -> Void) {
        Logger.log(message: "request: \(request)", event: .i)
        decodingTask(with: request, decodingType: T.self) { (json , error) in
            
            //MARK: change to main queue
            DispatchQueue.main.async {
                guard let json = json else {
                    if let error = error {
                        completion(Result.failure(error))
                    } else {
                        completion(Result.failure(.invalidData))
                    }
                    return
                }
                
                if let value = decode(json) {
                    completion(.success(value))
                } else {
                    completion(.failure(.jsonParsingFailure))
                }
            }
        }?.resume()
    }
    
    func cancelRequest() {
        if let dataTask = self.dataTask {
            dataTask.cancel()
        }
    }
}

enum TResult<Value> {
    case success(Value)
    case failure(Error)
}

enum APIError: Error {
    case noInternet
    case requestFailed
    case jsonConversionFailure
    case invalidData
    case responseUnsuccessful
    case jsonParsingFailure
    
    var localizedDescription: String {
        switch self {
        case .noInternet: return "The Internet connection appears to be offline."
        case .requestFailed: return "Request Failed. Please try again later"
        case .invalidData: return "Invalid Data. Please try again later"
        case .responseUnsuccessful: return "Response Unsuccessful. Please try again later"
        case .jsonParsingFailure: return "JSON Parsing Failure. Please try again later"
        case .jsonConversionFailure: return "JSON Conversion Failure. Please try again later"
        }
    }
}


public enum Result<T, U>  where U: Error{
    case success(T)
    case failure(U)
}
typealias ResultCallback<T> = (Result<T, APIError>) ->Void

protocol APIRequest: Encodable {
    associatedtype Response: Decodable
    var resourceName: String { get }
}

protocol APIResponse: Decodable {
    
}

struct TestHomeFeed: Decodable {
    let name: String?
}

struct GetHomeFeed:APIRequest {
    var resourceName: String {
        return "/api/home/homeFeeds"
    }
    typealias Response = [TestHomeFeed]
}


public enum DataType: CustomStringConvertible, Decodable {
    case string(String)
    case bool(Bool)
    case int(Int)
    case double(Double)
    
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        
        if let string = try? container.decode(String.self) {
            self = .string(string)
        }
        else if let bool = try? container.decode(Bool.self) {
            self = .bool(bool)
        }
        else if let int = try? container.decode(Int.self) {
            self = .int(int)
        }
        else if let double = try? container.decode(Double.self) {
            self = .double(double)
        }
        else {
            throw  MarvelError.decoding
        }
    }
    
    public var description: String {
        switch self {
        case .string(let string):
            return string
        case .bool(let bool):
            return String(describing: bool)
        case .int(let int):
            return String(describing: int)
        case .double(let double):
            return String(describing: double)
       
        }
    }
    
}


public enum MarvelError: Error {
    case encoding
    case decoding
    case server(message: String)
}
