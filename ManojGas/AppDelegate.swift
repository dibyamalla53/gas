//
//  AppDelegate.swift
//  ManojGas
//
//  Created by f1soft on 11/12/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import UIKit
import DropDown
import GoogleMaps
import UserNotifications
    enum AnimationType{
    case push
    case pop
    case present
    case dismiss
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
  
    
    func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            
            Logger.log(message: "Font Family Name = [\(familyName)]", event: .i)
            let names = UIFont.fontNames(forFamilyName: familyName )
            Logger.log(message: "Font Names = [\(names)]", event: .i)
        }
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //2
        GMSServices.provideAPIKey("AIzaSyDSIuABJFPaqaNEgNvWSec3HdHv1rMOdX0")
        // Override point for customization after application launch.
        //printFonts()
        Localizer.DoTheMagic()
        let currentAppleLanguage =  Language.currentAppleLanguage()
        let currentAppleLanguageFull = Language.currentAppleLanguageFull()
        Logger.log(message: "current app lang: \(currentAppleLanguage), currentAppleLanguageFull: \(currentAppleLanguageFull)", event: .i)
        
        DropDown.startListeningToKeyboard()
        registerForPushNotifications()
        
        return true
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data ) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        print("Device Token: \(token)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func setDrawerRootViewController(usingAnimation animation: AnimationType, andNotification notification: [AnyHashable: Any]? = nil) {
        Logger.log(message: "setDrawerRootViewController", event: .i)
        
        
        let mainNavVC = storyBoard.instantiateViewController(withIdentifier: "mainNav") as! CustomNavigationController
        let mainViewController = mainNavVC.viewControllers.first as! MainViewController
        mainViewController.notification = notification
        
        
        
        let currentViewController = self.window?.rootViewController
        if let  cvc = currentViewController {
            let width = cvc.view.frame.size.width
            let height = cvc.view.frame.size.height
            
            var newVCStartAnimationFrame: CGRect?
            var currentVCEndAnimationFrame: CGRect?
            var newVCAnimated = true
            switch animation
            {
            case .push:
                newVCStartAnimationFrame = CGRect(x: width, y: 0, width: width, height: height)
                currentVCEndAnimationFrame = CGRect(x: 0 - width/4, y: 0, width: width, height: height)
            case .pop:
                currentVCEndAnimationFrame = CGRect(x: width, y: 0, width: width, height: height)
                newVCStartAnimationFrame = CGRect(x: 0 - width/4, y: 0, width: width, height: height)
                newVCAnimated = false
            case .present:
                newVCStartAnimationFrame = CGRect(x: 0, y: height, width: width, height: height)
            case .dismiss:
                currentVCEndAnimationFrame = CGRect(x: 0, y: height, width: width, height: height)
                newVCAnimated = false
            }
            
            mainNavVC.view.frame = newVCStartAnimationFrame ?? CGRect(x: 0, y: 0, width: width, height: height)
            
            
            self.window?.addSubview(mainNavVC.view)
            if !newVCAnimated {
                self.window?.bringSubviewToFront(cvc.view)
            }
            UIView.animate(withDuration: 0.3, delay:0, options:[.curveEaseOut], animations: {
                if let cvcEndAnimationFrame = currentVCEndAnimationFrame {
                    cvc.view.frame = cvcEndAnimationFrame
                }
                mainNavVC.view.frame = CGRect(x: 0, y: 0, width: width, height: height)
                
            }) { (finish) in
                
                self.window?.rootViewController = mainNavVC
                if let rootVC = self.window?.rootViewController {
                    if rootVC.isKind(of: CustomNavigationController.self) {
                        let rootNav = rootVC as! CustomNavigationController
                        if let mainVC = rootNav.viewControllers.first as? MainViewController {
                            // mainVC.notification = nil
                        }
                    }
                }
            }
            
        }
        
    }
    
    func setUserViewControllerAsRoot(usingAnimation animation: AnimationType, andNotification notification: [AnyHashable: Any]? = nil) {
        Logger.log(message: "setDrawerRootViewController", event: .i)
        
        
        let mainNavVC = storyBoard.instantiateViewController(withIdentifier: "IniNav") as! CustomNavigationController
        let mainViewController = mainNavVC.viewControllers.first as! UserViewController
        mainViewController.notification = notification
        
        
        
        let currentViewController = self.window?.rootViewController
        if let  cvc = currentViewController {
            let width = cvc.view.frame.size.width
            let height = cvc.view.frame.size.height
            
            var newVCStartAnimationFrame: CGRect?
            var currentVCEndAnimationFrame: CGRect?
            var newVCAnimated = true
            switch animation
            {
            case .push:
                newVCStartAnimationFrame = CGRect(x: width, y: 0, width: width, height: height)
                currentVCEndAnimationFrame = CGRect(x: 0 - width/4, y: 0, width: width, height: height)
            case .pop:
                currentVCEndAnimationFrame = CGRect(x: width, y: 0, width: width, height: height)
                newVCStartAnimationFrame = CGRect(x: 0 - width/4, y: 0, width: width, height: height)
                newVCAnimated = false
            case .present:
                newVCStartAnimationFrame = CGRect(x: 0, y: height, width: width, height: height)
            case .dismiss:
                currentVCEndAnimationFrame = CGRect(x: 0, y: height, width: width, height: height)
                newVCAnimated = false
            }
            
            mainNavVC.view.frame = newVCStartAnimationFrame ?? CGRect(x: 0, y: 0, width: width, height: height)
            
            
            self.window?.addSubview(mainNavVC.view)
            if !newVCAnimated {
                self.window?.bringSubviewToFront(cvc.view)
            }
            UIView.animate(withDuration: 0.3, delay:0, options:[.curveEaseOut], animations: {
                if let cvcEndAnimationFrame = currentVCEndAnimationFrame {
                    cvc.view.frame = cvcEndAnimationFrame
                }
                mainNavVC.view.frame = CGRect(x: 0, y: 0, width: width, height: height)
                
            }) { (finish) in
                
                self.window?.rootViewController = mainNavVC
                if let rootVC = self.window?.rootViewController {
                    if rootVC.isKind(of: CustomNavigationController.self) {
                        let rootNav = rootVC as! CustomNavigationController
                        if let mainVC = rootNav.viewControllers.first as? UserViewController {
                            //mainVC.notification =
                        }
                    }
                }
            }
            
        }
        
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }

        }
    }
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current()
            .requestAuthorization(options: [.alert, .sound, .badge]) {
                [weak self] granted, error in
                
                print("Permission granted: \(granted)")
                guard granted else { return }
                self?.getNotificationSettings()
        }

    }
    


}
