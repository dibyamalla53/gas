//
//  EndPoint.swift
//  CineKancha
//
//  Created by Sagar Thapa on 10/31/18.
//  Copyright © 2018 Yuji Hato. All rights reserved.
//

import Foundation

protocol EndPoint {
    var base: String {get}
    var path: String{get}
}

extension EndPoint {
    var urlComponents: URLComponents {
        var components = URLComponents(string: base)!
        components.path = path
        return components
    }
    
    var request: URLRequest {
        let url = urlComponents.url!
        return URLRequest(url: url)
    }
}

enum ManojGas: String {
    case login
}

extension ManojGas: EndPoint {
    var base: String {
        return "http://manojgas.shirantechnologies.com"
    }
    
    var path: String {
        switch self {
        case .login: return "/api/login"
       
            
        }
    }
}

