//
//  SideMenuTableViewCell.swift
//  ManojGas
//
//  Created by f1soft on 11/13/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var menuLbl: UILabel!
    
    var model: SideMenuModel? {
        didSet {
            if let model = model {
                self.bindModel(image: model.image, menu: model.title)
            }
        }
    }
    
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    private func bindModel(image: UIImage, menu: String) {
        self.imgView.image = image
        self.menuLbl.text = menu
    }
    
}
