//
//  SideMenuViewModel.swift
//  ManojGas
//
//  Created by f1soft on 11/12/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import Foundation
import UIKit

struct SideMenuViewModel {
    
    var menus = [SideMenuModel]()
    let items = [["image":UIImage(named:"setting")!,"title":"Language Setting"],["image":UIImage(named:"safety-tips")!,"title":"Safety Tips"],["image":UIImage(named:"feedback")!,"title":"feedback"],["image":UIImage(named:"share")!,"title":"Share App"],["image":UIImage(named:"rate")!,"title":"Rate Us"],["image":UIImage(named:"contact-us")!,"title":"Contact Us"],["image":UIImage(named:"about-us")!,"title":"About Us"],["image":UIImage(named:"logout")!,"title":"Logout"]
    ]
    
    init() {
        menus.removeAll()
        for value in self.items {
            let menu = SideMenuModel(image:value["image"] as! UIImage, title: value["title"] as! String)
            menus.append(menu)
        }
    }
    
}
