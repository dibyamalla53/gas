//
//  SideViewController.swift
//  ManojGas
//
//  Created by f1soft on 11/12/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import UIKit

class SideViewController: UIViewController {
    var viewController: MainViewController?
    
    lazy var navigationBar: NavView = {
        let header = NavView()
        header.menuBtn.setImage(UIImage(named: "arrow-back"), for: .normal)
        header.titleLbl.isHidden = true
        header.menuBtn.addTarget(self, action: #selector(closeSideMenu), for: .touchUpInside)
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    
    
    let viewModel = SideMenuViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Logger.log(message: "menu items:\(viewModel.menus)", event:  .i)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         setupView()
    }
    
    @objc func closeSideMenu() {
        
        
    }
    
    private func setupView() {
        view.addSubview(navigationBar)
        view.addSubview(tableView)
        tableView.register(SideMenuTableViewCell.nib, forCellReuseIdentifier: SideMenuTableViewCell.identifier)
        setupContstraints()
    }
    
    

    private func setupContstraints() {
        var topConst: CGFloat = 20
        var navHeight: CGFloat = 44
        if hasTopNotch {
            topConst = 0
            navHeight *= 2
        }
        navigationBar.topAnchor.constraint(equalTo: view.topAnchor, constant:topConst).isActive = true
        navigationBar.leftAnchor.constraint(equalTo:  view.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo:  view.rightAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: navHeight).isActive = true
        
        tableView.topAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant:0).isActive = true
        tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }

}

extension SideViewController: UITableViewDataSource, UITableViewDelegate {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let menu  = viewModel.menus[indexPath.section]
        let cell = tableView.dequeueReusableCell(withIdentifier: SideMenuTableViewCell.identifier, for: indexPath) as! SideMenuTableViewCell
        cell.model = menu
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Logger.log(message: "selected row:\(indexPath.row)", event:.i)
    }
}

