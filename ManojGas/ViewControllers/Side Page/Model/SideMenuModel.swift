//
//  SideMenuModel.swift
//  ManojGas
//
//  Created by f1soft on 11/12/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import Foundation
import UIKit

struct SideMenuModel {
    let image: UIImage
    let title: String
}
