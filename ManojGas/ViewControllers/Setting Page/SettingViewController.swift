//
//  SettingViewController.swift
//  ManojGas
//
//  Created by f1soft on 11/15/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import UIKit
protocol SettingViewControllerDelegate {
    func okBtnTapped()
}

class SettingViewController: UIViewController {
    @IBOutlet weak var okBtn: UIButton!{
        didSet{
            let customerGrad = setGradient(topColor: UIColor(hexString: Color.CustomerButton.topGrad), bottomColor:UIColor(hexString: Color.CustomerButton.bottomGrad), frame:CGRect(x: 0, y: 0, width: 220, height: 40))
            okBtn.layer.insertSublayer(customerGrad, below: okBtn.titleLabel?.layer)
            okBtn.roundedBottomCorner()
        }
    }
    @IBOutlet weak var englishBtn: UIButton!{
        didSet{
            englishBtn.setImage(UIImage(named : "remember-me-selected"), for: .selected)
            englishBtn.setImage(UIImage(named : "remember-me-unselected"), for: .normal)
        }
    }
    @IBOutlet weak var nepaliBtn: UIButton!{
        didSet{
            nepaliBtn.setImage(UIImage(named : "remember-me-unselected"), for: .normal)
            nepaliBtn.setImage(UIImage(named : "remember-me-selected"), for: .selected)
        }
    }
    //var whichLang: String? = Helper().getValueForKey(key: Key.language)
    var delegate: SettingViewControllerDelegate?
    var whichLang = Language.currentAppleLanguage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
            switch whichLang {
            case LanguageOptions.english.rawValue:
                englishBtn.isSelected = true
            case LanguageOptions.nepali.rawValue:
                nepaliBtn.isSelected = true
            default:
                Logger.log(message: "case not matched", event: .i)
            }
        

    }
    
    
    @IBAction func englishBtnTapped(_ sender: UIButton) {
        if englishBtn.isSelected == true {
            nepaliBtn.isSelected = true
            englishBtn.isSelected = false
        }else {
            englishBtn.isSelected = true
             nepaliBtn.isSelected = false
        }
    }
    @IBAction func nepaliBtnTapped(_ sender: UIButton) {
        if nepaliBtn.isSelected == true {
            nepaliBtn.isSelected = false
            englishBtn.isSelected = true
        }else {
            nepaliBtn.isSelected = true
             englishBtn.isSelected = false
        }
    }
    
    @IBAction func okBtnTapped(_ sender: UIButton) {
        
        if nepaliBtn.isSelected {
            whichLang =  LanguageOptions.nepali.rawValue
        }else if englishBtn.isSelected {
            whichLang = LanguageOptions.english.rawValue
        }
        Language.setAppleLAnguageTo(lang: whichLang)
        Logger.log(message: "which lang: \(whichLang)", event: .i)
        //self.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true) {
           self.delegate?.okBtnTapped()
        }
        
    }
    @IBAction func dismissView(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
}
