//
//  SafetyTipsViewController.swift
//  ManojGas
//
//  Created by Mac on 4/10/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit
import WebKit

class SafetyTipsViewController: UIViewController {
    @IBOutlet weak var navBarContainerView: UIView!
    @IBOutlet weak var navBarContainerHC: NSLayoutConstraint!
    @IBOutlet weak var safetyImages: WKWebView!
    
    var safetyData = ""
    
    lazy var navigationBar: NavView = {
        let header = NavView()
        header.menuBtn.setImage(UIImage(named: "arrow-back"), for: .normal)
        header.titleLbl.isHidden = false
        header.contentView.backgroundColor = .clear
        header.msgSettingBtn.isHidden = true
        header.menuBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
    }()
    
    @objc func popView() {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        safetyTips()
    }
    
    func safetyTips(){
        ViewControllerUtils().showActivityIndicator(uiView: self.view)
        let jsonUrlString = GlobalKey.baseUrl + "/safetytips"
        guard let url = URL(string: jsonUrlString)
            else {
                return
        }
        
        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data
                else { return }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                if let jsonData = json as? [String: Any]{
                    if let myResults = jsonData["data"] as? String{
                        self.safetyData = myResults
                    }
                    DispatchQueue.main.async {
                        self.safetyImages.loadHTMLString(self.safetyData, baseURL: nil)
                        ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                    }
                }
            }
            catch{
                print(error)
            }
            }
            .resume()
    }
    
}

extension SafetyTipsViewController {
    func setupView() {
        navBarContainerView.addSubview(navigationBar)
        navigationBar.titleLbl.text = "Safety Tips"
        Logger.log(message: "map not loaded", event: .i)
        setupConstraints()
    }
    
    func setupConstraints(){
        var topConst: CGFloat = 20
        var navHeight: CGFloat = 44
        if hasTopNotch{
            topConst = 0
            navHeight *= 2
            navBarContainerHC.constant = navHeight
        }
        navigationBar.topAnchor.constraint(equalTo: navBarContainerView.topAnchor, constant: topConst).isActive = true
        navigationBar.leftAnchor.constraint(equalTo:  navBarContainerView.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo:  navBarContainerView.rightAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: navHeight).isActive = true
        
    }
}



