//
//  CustomSafetyViewController.swift
//  ManojGas
//
//  Created by f1soft mac on 5/30/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit

class CustomSafetyViewController: UIView {
    var buttons=[UIButton]()
    var selector:UIView!
    @IBInspectable
    var borderWidth:CGFloat = 0.0
    {
        didSet
          {
            
                layer.borderWidth=borderWidth
        }
    }
    
   @IBInspectable
    var borderColor:UIColor=UIColor.clear{
        didSet{
           
            layer.borderColor=borderColor as! CGColor
        }
    }
    
    @IBInspectable
    var commaSeperatedbuttonttiles:String=""
    {
        didSet{
            updateView()
        }
    }
    
     @IBInspectable
    var textColor:UIColor = .lightGray
    {
        didSet{
            updateView()
        }
    }
    
    @IBInspectable
    var selectorColor:UIColor = .darkGray
    {
        didSet{
            updateView()
        }
    }
    
    @IBInspectable
    var selectorTextColor:UIColor = .white
    {
        didSet{
            updateView()
        }
    }
    
    func updateView()
    {
        buttons.removeAll()
        subviews.forEach{
            (view) in
            view.removeFromSuperview()
        }
        let buttonTitles=commaSeperatedbuttonttiles.components(separatedBy: ",")
        for buttonTitle in buttonTitles
        {
            let button=UIButton(type:.system)
            button.setTitle(buttonTitle, for:.normal)
            button.setTitleColor(textColor, for:.normal)
            buttons.append(button)
        }
        buttons[0].setTitleColor(selectorTextColor, for:.normal)
        
        let selectorWidth=frame.width / CGFloat(buttonTitles.count)
        selector=UIView(frame:CGRect(x: 0, y: 0, width: selectorWidth, height: frame.height))
        selector.layer.cornerRadius=frame.height/2
        selector.backgroundColor=selectorColor
        addSubview(selector)
        
        let sv=UIStackView(arrangedSubviews: buttons)
        sv.axis = .horizontal
        sv.alignment = .fill
        sv.distribution = .fillProportionally
        addSubview(sv)
        sv.translatesAutoresizingMaskIntoConstraints=false
        sv.topAnchor.constraint(equalTo: self.topAnchor).isActive=true
        sv.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive=true
        sv.leftAnchor.constraint(equalTo: self.leftAnchor).isActive=true
        sv.rightAnchor.constraint(equalTo: self.rightAnchor).isActive=true
        
    }
    
 
    

   

}
