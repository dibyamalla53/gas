//
//  SafetyTipsV2ViewController.swift
//  ManojGas
//
//  Created by f1soft mac on 5/29/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit

class SafetyTipsV2ViewController: UIViewController {
    var safetyviewcontrolerone:UIView!
    var safetyviewcontrolertwo:UIView!

    @IBOutlet weak var viewContainer: UIView!
    @IBAction func changeScreen(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            viewContainer.bringSubviewToFront(safetyviewcontrolerone)
            break
        case 1:
            viewContainer.bringSubviewToFront(safetyviewcontrolertwo)
            break
            
        default:
            break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        safetyviewcontrolerone=SafetyTipsViewControllerOne().view
        safetyviewcontrolertwo=SafetyViewControllerTwo().view
        viewContainer.addSubview(safetyviewcontrolerone)
        viewContainer.addSubview(safetyviewcontrolertwo)
         viewContainer.bringSubviewToFront(safetyviewcontrolerone)
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
