//
//  SafetyViewControllerTwo.swift
//  ManojGas
//
//  Created by f1soft mac on 5/29/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit

class SafetyViewControllerTwo: UIViewController {

    @IBOutlet weak var scrollview: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollview.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(returnTextView)))
        // Do any additional setup after loading the view.
    }


    @objc func returnTextView() {
        view.endEditing(true)
        
        
    }
    

}
