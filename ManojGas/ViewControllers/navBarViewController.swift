//
//  navBarViewController.swift
//  ManojGas
//
//  Created by Mac on 4/15/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit

class navBarViewController: UIViewController {

    lazy var navigationBar: NavView = {
        let header = NavView()
        header.menuBtn.setImage(UIImage(named: "arrow-back"), for: .normal)
        header.titleLbl.isHidden = false
        header.contentView.backgroundColor = .clear
        header.messageSettingBtn.isHidden = true
        header.menuBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
    }()
    
    @objc func popView() {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func setupView() {
        navBarContainerView.addSubview(navigationBar)
        navigationBar.titleLbl.text = "My Map"
        Logger.log(message: "map not loaded", event: .i)
        setupConstraints()
    }
    
    func setupConstraints(){
        var topConst: CGFloat = 20
        var navHeight: CGFloat = 44
        if hasTopNotch{
            topConst = 0
            navHeight *= 2
            navBarContainerHC.constant = navHeight
        }
        navigationBar.topAnchor.constraint(equalTo: navBarContainerView.topAnchor, constant: topConst).isActive = true
        navigationBar.leftAnchor.constraint(equalTo:  navBarContainerView.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo:  navBarContainerView.rightAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: navHeight).isActive = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        // Do any additional setup after loading the view.
    }
    

}
