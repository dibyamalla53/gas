//
//  OrderDetailViewController.swift
//  ManojGas
//
//  Created by Mac on 12/4/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import UIKit

class OrderDetailViewController: UIViewController {
    var Status: String?
    var orderId: String?
    @IBOutlet weak var navBarContainerView: UIView!
    @IBOutlet weak var navBarContainerHC: NSLayoutConstraint!
    @IBOutlet weak var orderCode: UILabel!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var orderStat: UILabel!
    @IBOutlet weak var customerAddress: UILabel!
    @IBOutlet weak var contactNo: UILabel!
    @IBOutlet weak var requestDate: UILabel!
    @IBOutlet weak var respondedDate: UILabel!
    @IBOutlet weak var conDate: UILabel!
    @IBOutlet weak var deliDate: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBAction func closeButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        getDetails()
    }
    lazy var navigationBar: NavView = {
        let header = NavView()
        header.menuBtn.setImage(UIImage(named: "arrow-back"), for: .normal)
        header.titleLbl.isHidden = true
        header.msgSettingBtn.isHidden = true
        header.contentView.backgroundColor = .clear
        header.menuBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
    }()
    
    @objc func popView() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupView() {
        navBarContainerView.addSubview(navigationBar)
        
        setupConstraints()
    }
    
    func setupConstraints(){
        var topConst: CGFloat = 20
        var navHeight: CGFloat = 44
        if hasTopNotch{
            topConst = 0
            navHeight *= 2
            navBarContainerHC.constant = navHeight
        }
        navigationBar.topAnchor.constraint(equalTo: navBarContainerView.topAnchor, constant: topConst).isActive = true
        navigationBar.leftAnchor.constraint(equalTo:  navBarContainerView.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo:  navBarContainerView.rightAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: navHeight).isActive = true
        
    }
    
    func getDetails(){
        let url = URL(string: GlobalKey.baseUrl + "/order/detail")!
        let api_token = Helper().getValueForKey(key: GlobalKey.apiToken)
        let user = Helper().getValueForKey(key: Key.user)
        Logger.log(message: "orderId:\(String(describing: orderId))", event: .i)
        let parameterDictionary = [ "api_token": api_token,
                                    "order_id": orderId,
                                    "usertype": user]
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: []) else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil
                else {
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            if let jsonData = responseJSON as? [String : Any]
            {
                if let myResults = jsonData["data"] as? [[String : Any]]
                {
                    
                    for value in myResults
                    {
                        DispatchQueue.main.async {
                            self.orderCode.text = value["order_code"] as? String
                            self.customerName.text = value["customer_name"] as? String
                            self.customerAddress.text = value["customer_address"] as? String
                            self.contactNo.text = value["customer_mobileno"] as? String
                            self.requestDate.text = value["created_at"] as? String
                            self.respondedDate.text = value["confirmed_on"] as? String
                            self.conDate.text = value["order_sent_on"] as? String
                            self.deliDate.text = value["order_delivered_on"] as? String
                        }
                    }
                    
                }
            }
        }
        
        task.resume()
    }
    
    
}
