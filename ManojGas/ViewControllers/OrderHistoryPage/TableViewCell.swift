//
//  TableViewCell.swift
//  ManojGas
//
//  Created by Mac on 12/3/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var dealer: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var requestGas: UILabel!
    @IBOutlet weak var requestDate: UILabel!
    @IBOutlet weak var dealerImg: UIImageView!
    @IBOutlet weak var confirmBtn: UIButton!{
        didSet{
            let confirm = setGradient(topColor: UIColor(hexString: Color.DealerButton.topGrad), bottomColor: UIColor(hexString: Color.DealerButton.bottomGrad), frame: (CGRect(x: 0, y: 0, width: 80, height: 30)))
            
            confirmBtn.layer.insertSublayer(confirm, below: confirmBtn.titleLabel?.layer)
            confirmBtn.roundedCorners()
            
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
