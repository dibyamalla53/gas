//
//  OrderHistoryViewController.swift
//  ManojGas
//
//  Created by Mac on 12/2/18.
//  Copyright © 2018 shirantech. All rights reserved.


import UIKit


class OrderHistoryViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{
    
    var orderId: String?
    var buttonTitle = ""
    var sender: Int?
    var newOrder = NewOrder()
    var myTableViewDataSource = [HistoryInfo]()
    var orderResponseData = [HistoryInfo]()
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedTab == .orderRequest {
            return self.myTableViewDataSource.count
        }
        else if selectedTab == .orderResponse{
            return self.orderResponseData.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        if selectedTab == .orderRequest{
            
            cell.dealer.text = myTableViewDataSource[indexPath.row].dealerName
            cell.location.text = myTableViewDataSource[indexPath.row].dealerAddress
            cell.requestGas.text = " \(myTableViewDataSource[indexPath.row].quantity ?? 0)"
            cell.requestDate.text = myTableViewDataSource[indexPath.row].createdAt
            cell.confirmBtn.isHidden = true
           
        }
        else if selectedTab == .orderResponse
        {   
            cell.dealer.text = orderResponseData[indexPath.row].dealerName
            cell.location.text = orderResponseData[indexPath.row].dealerAddress
            cell.requestGas.text = "\(orderResponseData[indexPath.row].quantity ?? 0)"
            cell.requestDate.text = orderResponseData[indexPath.row].createdAt
            cell.confirmBtn.isHidden = false
            cell.confirmBtn.setTitle(orderResponseData[indexPath.row].orderStatus, for: .normal)
            cell.confirmBtn.setTitleColor(.white, for: .normal)
            cell.confirmBtn.addTarget(self, action: #selector(btnClicked), for: .touchUpInside)
        }
        cell.confirmBtn.tag = indexPath.row
        return cell
        
    }
    
    @objc func btnClicked(sender: UIButton){
        if let btnTitle = sender.currentTitle{
            buttonTitle = btnTitle.lowercased()
            if buttonTitle == "got it"{
                newOrder.translatesAutoresizingMaskIntoConstraints = false
                newOrder.removeFromSuperview()
                view.addSubview(newOrder)
                newOrder.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
                newOrder.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
                newOrder.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
                newOrder.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant:0).isActive = true
                newOrder.Yes.addTarget(self, action: #selector(yes(_:)), for: .touchUpInside)
                self.sender = sender.tag
            }
            else if buttonTitle == "complete"{
                let vc = storyBoard.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
                let index = sender.tag
                vc.orderId = orderResponseData[index].orderId
                Logger.log(message: "orderId:\(String(describing: orderResponseData[index].orderId))", event: .i)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @objc func yes(_ sender: UIButton){
        newOrder.removeFromSuperview()
        guard let index = self.sender else { return }
            self.orderId = orderResponseData[index].orderId
        
        processBtn()
        
    }
    
    func processBtn(){
        
        ViewControllerUtils().showActivityIndicator(uiView: self.view)
        let url = URL(string: GlobalKey.baseUrl + "/customers/order/delivered")!
        let token = Helper().getValueForKey(key: GlobalKey.apiToken)
        let parameterDictionary: [String: Any] = ["api_token" : token!, "order_id": self.orderId as Any , "order_type": "response"]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: [])
            else{
                return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil
                else{
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            
            
            let responseJson = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            if let jsonData = responseJson as? [String: Any]
            {
                if let myResults = jsonData["message"] as? String {
                   
                    DispatchQueue.main.async {
                         self.getData()
                        let alert = UIAlertController(title: "", message: myResults, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                    }
                    
                }
            }
            
        }
        task.resume()
    }
    
    lazy var navigationBar: NavView = {
        let header = NavView()
        header.menuBtn.setImage(UIImage(named: "arrow-back"), for: .normal)
        header.titleLbl.isHidden = true
        header.contentView.backgroundColor = .clear
        header.msgSettingBtn.isHidden = true
        header.menuBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
    }()
    @IBOutlet weak var navBarContainerView: UIView!
    @IBOutlet weak var navBarContainerHC: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.backgroundColor = .clear
        }
    }
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var responseBtn: UIButton!
    @IBOutlet var viewRes: UIView!
    @IBOutlet weak var requestBtn: UIButton!
    @IBOutlet weak var viewReq:UIView!
   
    
    
    enum OrderHistoryTab{
        case orderRequest, orderResponse
    }
    let api_token = Helper().getValueForKey(key: GlobalKey.apiToken)
    var selectedTab: OrderHistoryTab = .orderRequest
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getdata()
        getData()
        tableView.delegate = self
        tableView.dataSource = self
        setupViews()
        
        viewContainer.layer.borderWidth=1
        viewContainer.layer.borderColor = UIColor.lightGray.cgColor
        viewContainer.layer.cornerRadius=10
        
    }
    
    func getdata(){
        ViewControllerUtils().showActivityIndicator(uiView: self.view)
        var myOrders = HistoryInfo()
        
        let api_token = Helper().getValueForKey(key: GlobalKey.apiToken)
        
        let url = URL(string: GlobalKey.baseUrl + "/customers/orderhistory")!
    
        let json = ["api_token": api_token , "type": "request"]
      
        guard let jsonData = try? JSONSerialization.data(withJSONObject: json.self, options: []) else {
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil
                else {
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            if let jsonData = responseJSON as? [String : Any]
            {
                if let myResults = jsonData["data"] as? [[String : Any]]
                {
                    for value in myResults
                    {
                        if let dealerName = value["dealer_name"] as? String  {
                            myOrders.dealerName = dealerName
                        }
                        if let dealerAddress = value["customer_address"] as? String {
                            myOrders.dealerAddress = dealerAddress
                        }
                        if let quantity = value["quantity"] as? Int  {
                            myOrders.quantity = quantity
                        }
                        if let createdAt = value["created_at"] as? String {
                            myOrders.createdAt = createdAt
                        }
                        
                       
                        self.myTableViewDataSource.append(myOrders)
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                    }
                }
                
            }
            
        }
        
        task.resume()
    }
    
    func getData(){
        ViewControllerUtils().showActivityIndicator(uiView: self.view)
        var myOrders = HistoryInfo()
        
        let api_token = Helper().getValueForKey(key: GlobalKey.apiToken)
        
        let url = URL(string: "http://manojgas.shirantechnologies.com/api/customers/orderhistory")!
        
        let json = ["api_token": api_token , "type": "response"]
        
        Logger.log(message: "params: \(json), \(url)", event: .i)
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: json.self, options: []) else {
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil
                else {
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            if let jsonData = responseJSON as? [String : Any]
            {
                if let myResults = jsonData["data"] as? [[String : Any]]
                {self.orderResponseData.removeAll()
                    for value in myResults
                    {
                        if let dealerName = value["dealer_name"] as? String  {
                            myOrders.dealerName = dealerName
                        }
                        if let dealerAddress = value["customer_address"] as? String {
                            myOrders.dealerAddress = dealerAddress
                        }
                        if let quantity = value["quantity"] as? Int  {
                            myOrders.quantity = quantity
                        }
                        if let createdAt = value["created_at"] as? String {
                            myOrders.createdAt = createdAt
                        }
                        myOrders.orderStatus = value["order_status"] as? String
                        myOrders.orderId = value["order_id"] as? String
                        self.orderResponseData.append(myOrders)
                    
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                    }
                }
                
            }
            
        }
        
        task.resume()
    }
    
    
    @IBAction func orderRespBtn(_ sender: Any) {
        
        selectedTab = .orderResponse
        viewRes.backgroundColor = UIColor.red
        viewReq.backgroundColor = UIColor.clear
        self.tableView.reloadData()
    }

    @IBAction func orderReqBtn(_ sender: Any) {
        selectedTab = .orderRequest
        viewReq.backgroundColor = UIColor.red
        viewRes.backgroundColor = UIColor.clear
        self.tableView.reloadData()
        
    }
    
    @objc func popView() {
        self.navigationController?.popViewController(animated: true)
        
    }
    
}


extension OrderHistoryViewController{
    private func setupViews() {
        
        navBarContainerView.addSubview(navigationBar)
        setupConstraints()
    }
    
    private func setupConstraints() {
        
        
        var topConst: CGFloat = 20
        var navHeight: CGFloat = 44
        if hasTopNotch {
            topConst = 0
            navHeight *= 2
            navBarContainerHC.constant = navHeight
        }
        navigationBar.topAnchor.constraint(equalTo: navBarContainerView.topAnchor, constant:topConst).isActive = true
        navigationBar.leftAnchor.constraint(equalTo:  navBarContainerView.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo:  navBarContainerView.rightAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: navHeight).isActive = true
        
    }
}



