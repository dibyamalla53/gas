//
//  messageSetting.swift
//  ManojGas
//
//  Created by Mac on 4/17/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit
class messageSetting: UIView {
 
    @IBOutlet var contentView: UIView!{
        didSet{
            contentView.layer.cornerRadius = 10
        }
    }
    @IBOutlet weak var allDealers: UIButton!{
        didSet{
            allDealers.setImage(UIImage(named: "remember-me-selected") , for: .selected)
            allDealers.setImage(UIImage(named: "remember-me-unselected"), for: .normal)
        }
        
    }
    @IBAction func okBtn(_ sender: UIButton) {
        self.removeFromSuperview()
    }

    @IBOutlet weak var single: UIButton!{
        didSet{
            single.setImage(UIImage(named: "remember-me-selected"), for: .selected)
            single.setImage(UIImage(named: "remember-me-unselected"), for: .normal)
        }
    }
    
    @IBAction func allDealers(_ sender: UIButton) {
        if allDealers.isSelected == true {
            single.isSelected = true
            allDealers.isSelected = false
        }else {
            allDealers.isSelected = true
            single.isSelected = false
        }
        
    }
    
    @IBAction func single(_ sender: UIButton) {
        if single.isSelected == true {
            single.isSelected = false
            allDealers.isSelected = true
        }else {
            single.isSelected = true
            allDealers.isSelected = false
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    func setupViews() {
    Bundle.main.loadNibNamed("messageSetting", owner: self, options: nil)
        addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        contentView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant:0).isActive = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        contentView.addGestureRecognizer(tapGesture)
        
    }
    @objc func handleTap(sender: UITapGestureRecognizer) {
        self.removeFromSuperview()
        
    }
    
}

