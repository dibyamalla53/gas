//
//  CustomerDashboardFeatureViewModel.swift
//  ManojGas
//
//  Created by f1soft on 11/14/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import Foundation
import UIKit

struct CustomerDashboardFeatureViewModel {
    
    var menus = [CustomerDashboardFeatureModel]()
    let items = [["image":UIImage(named:"gas-red")!,"title":multipleColorText(text: "Order History", range: NSRange(location: 6, length: 7), color: .black, font: UIFont(name: Font.MyriadProSemibold, size: Font.fontSize14))],["image":UIImage(named:"dealer-red")!,"title":multipleColorText(text: "My Dealers", range: NSRange(location: 3, length: 7), color: .black, font: UIFont(name: Font.MyriadProSemibold, size: Font.fontSize14))],["image":UIImage(named:"dash-safety-tips")!,"title":multipleColorText(text: "Safety Tips", range: NSRange(location: 7, length: 4), color: .black, font: UIFont(name: Font.MyriadProSemibold, size: Font.fontSize14))],["image":UIImage(named:"share-app")!,"title":multipleColorText(text: "Share App", range: NSRange(location: 6, length: 3), color: .black, font: UIFont(name: Font.MyriadProSemibold, size: Font.fontSize14))],["image":UIImage(named:"rate-app")!,"title":multipleColorText(text: "Rate App", range: NSRange(location: 5, length: 3), color: .black, font: UIFont(name: Font.MyriadProSemibold, size: Font.fontSize14))]]
    
    
    init() {
        menus.removeAll()
        for value in self.items {
            let menu = CustomerDashboardFeatureModel(image:value["image"] as! UIImage, title: value["title"] as! NSMutableAttributedString)
            menus.append(menu)
        }
    }
    
}
