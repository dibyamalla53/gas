//
//  LocateDistributorViewController.swift
//  ManojGas
//
//  Created by Mac on 4/23/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class LocateDistributorViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var navBarContainerView: UIView!
    @IBOutlet weak var navBarContainerHC: NSLayoutConstraint!
    @IBOutlet weak var mapView: UIView!
    var dealerLocation = locations()
    var allDealers = [locations]()
    var camera: GMSCameraPosition!
    var gmsMapView: GMSMapView!
    var latitude: Double?
    var longitude: Double?
    
    lazy var navigationBar: NavView = {
        let header = NavView()
        header.menuBtn.setImage(UIImage(named: "arrow-back"), for: .normal)
        header.titleLbl.isHidden = true
        header.msgSettingBtn.isHidden = true
        header.contentView.backgroundColor = .clear
        header.menuBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
    }()
    
    @objc func popView() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupView() {
        navBarContainerView.addSubview(navigationBar)
        setupConstraints()
    }
    
    let locationManager = CLLocationManager()
    
    func setupConstraints(){
        var topConst: CGFloat = 20
        var navHeight: CGFloat = 44
        if hasTopNotch{
            topConst = 0
            navHeight *= 2
            navBarContainerHC.constant = navHeight
        }
        navigationBar.topAnchor.constraint(equalTo: navBarContainerView.topAnchor, constant: topConst).isActive = true
        navigationBar.leftAnchor.constraint(equalTo:  navBarContainerView.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo:  navBarContainerView.rightAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: navHeight).isActive = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        setupView()
        locateDealer()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first
        {
            //let latitude = location.coordinate.latitude
            self.latitude = 27.704719742426843
            Helper().setDoubleValue(value: self.latitude!, key: GlobalKey.latitude)
           // let longitude = location.coordinate.longitude
            self.longitude = 85.32161495372009
            Helper().setDoubleValue(value: self.longitude!, key: GlobalKey.longitude)
            
            camera = GMSCameraPosition.camera(withLatitude: self.latitude! , longitude: self.longitude! , zoom: 14)
            gmsMapView = GMSMapView.map(withFrame: CGRect(x:0, y:0, width: self.mapView.frame.size.width, height: self.mapView.frame.size.height), camera: camera)
            gmsMapView.settings.myLocationButton = true
            gmsMapView.isMyLocationEnabled = true
            let location = CLLocationCoordinate2D(latitude: self.latitude!, longitude: self.longitude!)
            let marker = GMSMarker()
            marker.position = location
            marker.snippet = "User"
            marker.appearAnimation = GMSMarkerAnimation.pop
            marker.map = gmsMapView
            marker.icon = GMSMarker.markerImage(with: .green)
            self.mapView.addSubview(gmsMapView)

         }
    }
    
    func showCurrentLocationOnMap(){
     //   let camera = GMSCameraPosition.camera(withLatitude: (self.locationManager.location?.coordinate.latitude)!, longitude: (self.locationManager.location?.coordinate.longitude)!, zoom: 14)
        
        
        for dealer in allDealers{
            let latitude = dealer.latitude!
            let longitude = dealer.longitude!
            Logger.log(message: "\(String(describing: dealer.latitude)) & \(String(describing: dealer.longitude))", event: .i)
            
            Logger.log(message: "\(dealer)", event: .i)
            let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let marker = GMSMarker()
            marker.position = location
            marker.snippet = dealer.name
            marker.appearAnimation = GMSMarkerAnimation.pop
            marker.map = gmsMapView
  
        }
    }
    
    func locateDealer(){
        let url = URL(string: GlobalKey.baseUrl + "/search/dealer")!
        let token = Helper().getValueForKey(key: GlobalKey.apiToken)
        let latitude = 27.704719742426843
        let longitude = 85.32161495372009
        let parameterDictionary = ["api_token": token as Any,
                                   "latitude": latitude as Any,
                                   "longitude": longitude as Any] as [String : Any]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: []) else {
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil
                else {
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            if let jsonData = responseJSON as? [String : Any]
            {
                if let myResults = jsonData["data"] as? [[String : Any]]
                {
                    Logger.log(message: "result", event: .i)
                    for value in myResults{
                        self.dealerLocation.uuid = value["uuid"] as? String
                        self.dealerLocation.name = value["name"] as? String
                        if let val = value["latitude"] as? String {
                            let lat = Double(val)
                            self.dealerLocation.latitude = lat
                        }
                        if let lon = value["longitude"] as? String {
                            let longitude = Double(lon)
                            self.dealerLocation.longitude = longitude
                        }
                        
                        self.allDealers.append(self.dealerLocation)
                       dump(self.dealerLocation)
                    }
                    
                }
                
                DispatchQueue.main.async {
                    
                    self.showCurrentLocationOnMap()
                        ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                    }
                    if let myError = jsonData["status"] as? String{
                        if myError == "ERROR" {
                            if let message = jsonData["message"] as? String{
                                
                                DispatchQueue.main.async {
                                    let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                                        action in
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                    ))
                                    ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                                    self.present(alert, animated: true, completion: nil)
                                }}
                        }
                    }
                }
                
            }
            task.resume()
        
    
    }

}
