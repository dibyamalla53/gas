//
//  NewOrder.swift
//  ManojGas
//
//  Created by Mac on 6/9/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit

class NewOrder: UIView{

    @IBOutlet weak var Yes: UIButton!
    @IBOutlet weak var No: UIButton!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var popUp: UIView!
    @IBAction func No(_ sender: UIButton) {
        self.removeFromSuperview()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    
    func setupViews() {
        Bundle.main.loadNibNamed("NewOrder", owner: self, options: nil)
        addSubview(contentView)
        Logger.log(message: "into the new order", event: .i)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        contentView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        contentView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant:0).isActive = true
        
        popUp.layer.borderColor = UIColor.lightGray.cgColor
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        self.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
       self.removeFromSuperview()
        
    }
   

}
