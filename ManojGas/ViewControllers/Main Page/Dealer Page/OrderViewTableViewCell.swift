//
//  OrderViewTableViewCell.swift
//  ManojGas
//
//  Created by Mac on 6/6/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit

class OrderViewTableViewCell: UITableViewCell {
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var confirmBttn: UIButton!{
        didSet{
            let confirm = setGradient(topColor: UIColor(hexString: Color.DealerButton.topGrad), bottomColor: UIColor(hexString: Color.DealerButton.bottomGrad), frame: (CGRect(x: 0, y: 0, width: 80, height: 30)))
            
            confirmBttn.layer.insertSublayer(confirm, below: confirmBttn.titleLabel?.layer)
            confirmBttn.setTitleColor(.white, for: .normal)
            confirmBttn.roundedCorners()
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
