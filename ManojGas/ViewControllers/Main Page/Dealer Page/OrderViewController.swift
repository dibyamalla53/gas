//
//  OrderHistoryViewController.swift
//  ManojGas
//
//  Created by Mac on 4/24/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit

class OrderViewController: UIViewController {
    
    enum OrderHistoryTab{
        case orderRequest, orderResponse
    }
    var buttonLabel = OrderViewTableViewCell()
    var orderId: String?
    var orderCode: String?
    var sender: Int?
    var buttonTitle = ""
    var newOrder = NewOrder()
    var myTableViewDataSource = [HistoryInfo]()
    var orderResponseData = [HistoryInfo]()
    var selectedTab: OrderHistoryTab = .orderRequest
    @IBOutlet weak var navBarContainerView: UIView!
    @IBOutlet weak var navBarContainerHC: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var response: UIButton!
    @IBOutlet weak var request: UIButton!
    @IBOutlet weak var requestView: UIView!
    @IBOutlet weak var responseView: UIView!
    @IBAction func request(_ sender: UIButton) {
        selectedTab = .orderRequest
        requestView.backgroundColor = UIColor.red
        responseView.backgroundColor = UIColor.white
        self.tableView.reloadData()
    }
    @IBAction func response(_ sender: UIButton) {
        selectedTab = .orderResponse
        responseView.backgroundColor = UIColor.red
        requestView.backgroundColor = UIColor.clear
        self.tableView.reloadData()
    }
    
    @IBOutlet weak var segmentView: UIView!
    lazy var navigationBar: NavView = {
        let header = NavView()
        header.menuBtn.setImage(UIImage(named: "arrow-back"), for: .normal)
        header.titleLbl.isHidden = true
        header.msgSettingBtn.isHidden = true
        header.contentView.backgroundColor = .clear
        header.menuBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
    }()
    
    @objc func popView() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupView() {
        navBarContainerView.addSubview(navigationBar)
        setupConstraints()
    }
    
    func setupConstraints(){
        var topConst: CGFloat = 20
        var navHeight: CGFloat = 44
        if hasTopNotch{
            topConst = 0
            navHeight *= 2
            navBarContainerHC.constant = navHeight
        }
        navigationBar.topAnchor.constraint(equalTo: navBarContainerView.topAnchor, constant: topConst).isActive = true
        navigationBar.leftAnchor.constraint(equalTo:  navBarContainerView.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo:  navBarContainerView.rightAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: navHeight).isActive = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        tableView.delegate = self
        tableView.dataSource = self
        getResquest()
        getResponse()
    }

}

extension OrderViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedTab == .orderRequest {
            return self.myTableViewDataSource.count
        }
        else if selectedTab == .orderResponse{
            return self.orderResponseData.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OrderViewTableViewCell
        cell.confirmBttn.addTarget(self, action: #selector(btnClicked), for: .touchUpInside)
    
        if selectedTab == .orderRequest{
            cell.customerName.text =  myTableViewDataSource[indexPath.row].dealerName
            cell.address.text = myTableViewDataSource[indexPath.row].dealerAddress
            cell.quantity.text = "Requested for total" + " " + "\(myTableViewDataSource[indexPath.row].quantity ?? 0)" + " " + "Gases"
            cell.date.text = "Requested date" + " " + myTableViewDataSource[indexPath.row].createdAt!
            cell.confirmBttn.setTitle("CONFIRM", for: .normal)
        }
        else if selectedTab == .orderResponse
        {
            cell.customerName.text = orderResponseData[indexPath.row].dealerName
            cell.address.text = orderResponseData[indexPath.row].dealerAddress
            cell.quantity.text = "Requested for total" + " " + "\(orderResponseData[indexPath.row].quantity ?? 0)" + " " + "Gases"
            cell.date.text = "Requested date" + " " + orderResponseData[indexPath.row].createdAt!
            cell.confirmBttn.setTitle(orderResponseData[indexPath.row].orderStatus, for: .normal)
        }
        cell.confirmBttn.tag = indexPath.row
        return cell
    }
    
    @objc func btnClicked(sender: UIButton){
        if let btnTitle = sender.currentTitle{
             buttonTitle = btnTitle.uppercased()
            
            if buttonTitle == "PROCESS" || buttonTitle == "CONFIRM"  {
                
                newOrder.translatesAutoresizingMaskIntoConstraints = false
                newOrder.removeFromSuperview()
                view.addSubview(newOrder)
                newOrder.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
                newOrder.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
                newOrder.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
                newOrder.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant:0).isActive = true
                newOrder.Yes.addTarget(self, action: #selector(yes(_:)), for: .touchUpInside)
                self.sender = sender.tag
            }
            else if buttonTitle == "ON THE WAY" {}
                
            else if buttonTitle == "COMPLETE" {
                let vc = storyBoard.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
                let index = sender.tag
                vc.orderId = orderResponseData[index].orderId
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @objc func yes(_ sender: UIButton){
        newOrder.removeFromSuperview()
        Logger.log(message: "self.sender :\(String(describing: self.sender))", event: .i)
        guard let index = self.sender else { return }
        
        if selectedTab == .orderRequest {
            self.orderId = myTableViewDataSource[index].orderId
            self.orderCode = myTableViewDataSource[index].orderCode
            orderRequest()
        }
        else if selectedTab == .orderResponse {
            self.orderId = orderResponseData[index].orderId
            Logger.log(message: "position\(String(describing: self.orderId))", event: .i)
            orderResponse()
        }
    }
    
    func orderComplete(){
        let vc = storyBoard.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func orderResponse(){
        ViewControllerUtils().showActivityIndicator(uiView: self.view)
        let url = URL(string: GlobalKey.baseUrl + "/dealers/order/onProcess")!
        let token = Helper().getValueForKey(key: GlobalKey.apiToken)
        let parameterDictionary: [String: Any] = ["api_token" : token!, "order_id": self.orderId as Any , "order_type": "response"]
        Logger.log(message: "orderid:\(String(describing: self.orderId)) & \(String(describing: self.sender))", event: .i)
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: [])
            else{
                return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil
                else{
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            
           
                let responseJson = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                if let jsonData = responseJson as? [String: Any]
                {
                    if let myResults = jsonData["message"] as? String {
                        
                        DispatchQueue.main.async {
                            self.getResponse()
                            let alert = UIAlertController(title: "", message: myResults, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                        }
                       
                    }
                }
            
        }
        task.resume()
    }
    func orderRequest(){
        ViewControllerUtils().showActivityIndicator(uiView: self.view)
        let url = URL(string: GlobalKey.baseUrl + "/dealers/confirm/order")!
        let token = Helper().getValueForKey(key: GlobalKey.apiToken)
        let parameterDictionary: [String: Any] = ["api_token" : token!, "order_id": self.orderId as Any , "order_code": self.orderCode as Any, "order_status": "response"]
        Logger.log(message: "orderid:\(String(describing: self.orderId)) & \(String(describing: self.sender))", event: .i)
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: [])
            else{
                return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil
                else{
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            
            DispatchQueue.main.async {
                let responseJson = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                if let jsonData = responseJson as? [String: Any]
                {
                    if let myResults = jsonData["message"] as? String {
                        let alert = UIAlertController(title: "", message: myResults, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                            action in
                            
                            self.newOrder.removeFromSuperview()
                            self.myTableViewDataSource.remove(at: self.sender!)
                            Logger.log(message: "tag\(String(describing: self.sender))", event: .i)
                            self.tableView.reloadData()
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                ViewControllerUtils().hideActivityIndicator(uiView: self.view)
            }
        }
        task.resume()
    }
    func getResquest(){
        ViewControllerUtils().showActivityIndicator(uiView: self.view)
        var myOrders = HistoryInfo()
        let api_token = Helper().getValueForKey(key: GlobalKey.apiToken)
        let url = URL(string: GlobalKey.baseUrl + "/dealers/orderhistory")!
        let parameterDictionary = ["api_token": api_token, "type": "request"]
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: []) else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil
                else {
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            if let jsonData = responseJSON as? [String : Any]
            {
                if let myResults = jsonData["data"] as? [[String : Any]]
                {
                    for value in myResults
                    {
                        if let customerName = value["customer_name"] as? String  {
                            myOrders.dealerName = customerName
                        }
                        
                        if let customerAddress = value["customer_address"] as? String {
                            myOrders.dealerAddress = customerAddress
                        }
                        if let quantity = value["quantity"] as? Int  {
                            myOrders.quantity = quantity
                        }
                        if let createdAt = value["created_at"] as? String {
                            myOrders.createdAt = createdAt
                        }
                        if let active = value["active"] as? String{
                            myOrders.active = active
                        }
                        
                        myOrders.orderId = value["order_id"] as? String
                        myOrders.orderCode = value["order_code"] as? String
                        self.myTableViewDataSource.append(myOrders)
                        
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                    }
                }
                
            }
        }
        
        task.resume()
        
    }
    func getResponse(){
        ViewControllerUtils().showActivityIndicator(uiView: self.view)
        var myResponse = HistoryInfo()
        let api_token = Helper().getValueForKey(key: GlobalKey.apiToken)
        let url = URL(string: GlobalKey.baseUrl + "/dealers/orderhistory")!
        let parameterDictionary = ["api_token": api_token, "type": "response"]
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: []) else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil
                else {
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            if let jsonData = responseJSON as? [String : Any]
            {
                if let myResults = jsonData["data"] as? [[String : Any]]
                {
                    self.orderResponseData.removeAll()
                    for value in myResults
                    {
                        if let customerName = value["customer_name"] as? String  {
                            myResponse.dealerName = customerName
                        }
                        
                        if let customerAddress = value["customer_address"] as? String {
                            myResponse.dealerAddress = customerAddress
                        }
                        if let quantity = value["quantity"] as? Int  {
                            myResponse.quantity = quantity
                        }
                        if let createdAt = value["created_at"] as? String {
                            myResponse.createdAt = createdAt
                        }
                        if let active = value["active"] as? String{
                            myResponse.active = active
                        }
                        if let orderId = value["order_id"] as? String{
                            myResponse.orderId = orderId
                        }
                        
                        myResponse.orderCode = value["order_code"] as? String
                        if let status = value["order_status"] as? String{
                            myResponse.orderStatus = status.uppercased()
                        }
                       
                        self.orderResponseData.append(myResponse)
                        
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                    }
                }
                
            }
        }
        
        task.resume()
        
    }
}

