//
//  NewOrderViewController.swift
//  ManojGas
//
//  Created by Mac on 4/24/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit

class NewOrderViewController: UIViewController {
    
    var order = HistoryInfo()
    var orderTab = [HistoryInfo]()
    var newOrder = NewOrder()
    var orderId: String?
    var orderCode: String?
    var sender: Int?
    @IBOutlet weak var navBarContainerView: UIView!
    @IBOutlet weak var navBarContainerHC: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    
    lazy var navigationBar: NavView = {
        let header = NavView()
        header.menuBtn.setImage(UIImage(named: "arrow-back"), for: .normal)
        header.titleLbl.isHidden = true
        header.msgSettingBtn.isHidden = true
        header.contentView.backgroundColor = .clear
        header.menuBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        orderList()
        tableView.delegate = self
        tableView.dataSource = self
    }
    @objc func popView() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupView() {
        navBarContainerView.addSubview(navigationBar)
        setupConstraints()
    }
    
    func setupConstraints(){
        var topConst: CGFloat = 20
        var navHeight: CGFloat = 44
        if hasTopNotch{
            topConst = 0
            navHeight *= 2
            navBarContainerHC.constant = navHeight
        }
        navigationBar.topAnchor.constraint(equalTo: navBarContainerView.topAnchor, constant: topConst).isActive = true
        navigationBar.leftAnchor.constraint(equalTo:  navBarContainerView.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo:  navBarContainerView.rightAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: navHeight).isActive = true
        
    }
    
}

extension NewOrderViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderTab.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NewOrderTableViewCell
        cell.dealerName.text = orderTab[indexPath.row].dealerName
        cell.address.text = orderTab[indexPath.row].dealerAddress
        cell.quantity.text = "Requested for total" + " " + "\(orderTab[indexPath.row].quantity ?? 0)" + " " + "Gases"
        cell.date.text = "Requested date" + " " + orderTab[indexPath.row].createdAt!
        cell.confirmBtn.tag = indexPath.row
        cell.confirmBtn.addTarget(self, action: #selector(popUp), for: .touchUpInside)
        return cell
    }
    
    @objc func popUp(sender: UIButton){
        self.orderId = orderTab[sender.tag].orderId
        self.orderCode = orderTab[sender.tag].orderCode
        self.sender = sender.tag
        Logger.log(message: "tag:\(sender.tag)", event: .i)
        newOrder.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(newOrder)
        newOrder.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        newOrder.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        newOrder.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        newOrder.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant:0).isActive = true
        newOrder.Yes.addTarget(self, action: #selector(yes), for: .touchUpInside)

    }
    
    @objc func yes(){
        
        let url = URL(string: GlobalKey.baseUrl + "/dealers/confirm/order")!
        
        let token = Helper().getValueForKey(key: GlobalKey.apiToken)
        
        let parameterDictionary: [String: Any] = ["api_token" : token!, "order_id": self.orderId as Any , "order_code": self.orderCode as Any, "order_status": "response"]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: [])
            else{
                return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil
                else{
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            
            
            DispatchQueue.main.async {
                let responseJson = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                if let jsonData = responseJson as? [String: Any]
                {
                    if let myResults = jsonData["message"] as? String {
                        let alert = UIAlertController(title: "", message: myResults, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                           action in
                            
                            self.newOrder.removeFromSuperview()
                            self.orderTab.remove(at: self.sender!)
                            Logger.log(message: "tag\(String(describing: self.sender))", event: .i)
                            self.tableView.reloadData()
                            
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        task.resume()
    }
    
    func orderList(){
        ViewControllerUtils().showActivityIndicator(uiView: self.view)
        let api_token = Helper().getValueForKey(key: GlobalKey.apiToken)
        let url = URL(string: GlobalKey.baseUrl + "/dealers/order/list/new")!
        let parameterDictionary = ["api_token": api_token]
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: []) else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil
                else {
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            if let jsonData = responseJSON as? [String : Any]
            {
                if let myResults = jsonData["data"] as? [[String : Any]]
                {
                    for value in myResults
                    {
                        if let customerName = value["customer_name"] as? String  {
                            self.order.dealerName = customerName
                        }
                        
                        if let customerAddress = value["customer_address"] as? String {
                            self.order.dealerAddress = customerAddress
                        }
                        if let quantity = value["quantity"] as? Int  {
                            self.order.quantity = quantity
                        }
                        if let createdAt = value["created_at"] as? String {
                            self.order.createdAt = createdAt
                        }
                        if let active = value["active"] as? String{
                            self.order.active = active
                        }
                        
                        self.order.orderId = value["order_id"] as? String
                        self.order.orderCode = value["order_code"] as? String
                        self.orderTab.append(self.order)
                        
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                    }
                }
                
            }
        }
        
        task.resume()
        
    }
    
}


