//
//  NewOrderTableViewCell.swift
//  ManojGas
//
//  Created by Mac on 6/6/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit

class NewOrderTableViewCell: UITableViewCell {
    @IBOutlet weak var dealerName: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var confirmBtn: UIButton!{
        didSet{
            
            let confirmBttn = setGradient(topColor: UIColor(hexString: Color.DealerButton.topGrad), bottomColor: UIColor(hexString: Color.DealerButton.bottomGrad), frame: (CGRect(x: 0, y: 0, width: 80, height: 30)))
            
            confirmBtn.layer.insertSublayer(confirmBttn, below: confirmBtn.titleLabel?.layer)
            confirmBtn.roundedCorners()
            confirmBtn.setTitleColor(.white, for: .normal)
        
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
