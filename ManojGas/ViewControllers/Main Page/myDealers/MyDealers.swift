//
//  MyDealers.swift
//  ManojGas
//
//  Created by Mac on 4/18/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit

class MyDealers: UIView {
    
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var textLabel: UILabel!
    
    @IBAction func indexChanged(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            textLabel.text = "First Segment Selected"
        case 1:
            textLabel.text = "Second Segment Selected"
        default:
            break
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    func setupViews() {
        Bundle.main.loadNibNamed("myDealers", owner: self, options: nil)
        addSubview(contentView)
       
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        contentView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant:0).isActive = true
        
    }
}
