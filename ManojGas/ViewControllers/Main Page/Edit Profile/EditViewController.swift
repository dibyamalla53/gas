import UIKit

protocol NameChagedDelegate {
    func updateName(name: String)
}
class EditViewController: UIViewController {
    var newName: NameChagedDelegate!
    @IBOutlet weak var navBarContainerView: UIView!
    @IBOutlet weak var navBarContainerHC: NSLayoutConstraint!
    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var addressLabel: UITextField!
    @IBOutlet weak var mobileLabel: UITextField!
    @IBOutlet weak var editButton: UIButton!
    @IBAction func editButton(_ sender: UIButton) {
        
        ViewControllerUtils().showActivityIndicator(uiView: self.view)
        let editname = nameLabel.text!
        let editaddress = addressLabel.text!
        let editmobile = mobileLabel.text!
        Logger.log(message: "editname:\(editname)", event: .i)
        let user = Helper().getValueForKey(key: Key.user)
        let url = URL(string: GlobalKey.baseUrl + "/edit/details")!
        let apitoken = Helper().getValueForKey(key: GlobalKey.apiToken)!
        let lat: Double = Helper().getDoubleValue(forKey: GlobalKey.latitude)!
        let lon: Double = Helper().getDoubleValue(forKey: GlobalKey.longitude)!
        let parameterDictionary: [String: Any] = ["api_token": apitoken,
                                                  "usertype":user as Any,
                                                  "name": editname,
                                                  "address": editaddress,
                                                  "mobileno":editmobile,
                                                  "latitude":lat,
                                                  "longitude":lon
                                                ]
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                
                print("this is \(response)")
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    if let data = json as? [String: Any]{
                        Logger.log(message: "data:: \(data)", event: .i)
                    let message=data["message"] as? String
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                        action in
                            self.navigationController?.popToRootViewController(animated: true)
                        }))
                        self.present(alert, animated: true)
                        
                        self.nameLabel.text = editname
                        self.addressLabel.text = editaddress
                        self.mobileLabel.text = editmobile
                        Helper().setValue(value:editname,key: GlobalKey.customername)
                        Helper().setValue(value:editaddress,key: GlobalKey.customeraddress)
                        Helper().setValue(value:editmobile,key: GlobalKey.phoneno)
                        self.newName.updateName(name: editname)
                        ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                    }
                    }
                }catch {
                    print(error)
                }
            }
            }.resume()
     
    }
    
    lazy var navigationBar: NavView = {
        let header = NavView()
        header.menuBtn.setImage(UIImage(named: "arrow-back"), for: .normal)
        header.titleLbl.isHidden = true
        header.contentView.backgroundColor = .clear
        header.msgSettingBtn.isHidden = true
        header.menuBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
    }()
    
    @objc func popView() {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func setupView() {
        navBarContainerView.addSubview(navigationBar)
        Logger.log(message: "map not loaded", event: .i)
        setupConstraints()
    }
    
    func setupConstraints(){
        var topConst: CGFloat = 20
        var navHeight: CGFloat = 44
        if hasTopNotch{
            topConst = 0
            navHeight *= 2
            navBarContainerHC.constant = navHeight
        }
        navigationBar.topAnchor.constraint(equalTo: navBarContainerView.topAnchor, constant: topConst).isActive = true
        navigationBar.leftAnchor.constraint(equalTo:  navBarContainerView.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo:  navBarContainerView.rightAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: navHeight).isActive = true
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupView()
        let address = Helper().getValueForKey(key: GlobalKey.customeraddress)
        let name = Helper().getValueForKey(key: GlobalKey.customername)
        let mobilenumber = Helper().getValueForKey(key: GlobalKey.phoneno)
        nameLabel.text=name
        addressLabel.text=address
        mobileLabel.text=mobilenumber

    }
    
   
    
}

