//
//  MyDealersViewController.swift
//  ManojGas
//
//  Created by Mac on 4/19/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit
//var CustomSegmentControl = CustomSegmentControl()

class MyDealersViewController: UIViewController {
    var dealerList = Dealers()
    var myDealers = [Dealers]()
    var myBlockList = [Dealers]()
    var saved = [String]()
    @IBOutlet weak var navBarContainerView: UIView!
    @IBOutlet weak var navBarContainerHC: NSLayoutConstraint!
    @IBOutlet weak var CustomSegmentControl: CustomSegmentControl!
    @IBOutlet weak var selectDealersBtn: UIButton!{
        didSet{
            let customerGrad = setGradient(topColor: UIColor(hexString: Color.CustomerButton.topGrad), bottomColor:UIColor(hexString: Color.CustomerButton.bottomGrad), frame:CGRect(x: 0, y: 0, width: screenSize.width-40, height: 40), cornerRadius: 20)
            selectDealersBtn.layer.insertSublayer(customerGrad, below: selectDealersBtn.titleLabel?.layer)
            selectDealersBtn.setTitleColor(.white, for: .normal)
        
        }
    }

    @IBAction func selectDealersBtn(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditDealerViewController") as! EditDealerViewController
        vc.identifier = CustomSegmentControl.selectedSegmentIndex
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func segmentedControl(_ sender: CustomSegmentControl){
        switch sender.selectedSegmentIndex {
        case 0:
            sender.selectedSegmentIndex = 0
            self.tableView.reloadData()
            selectDealersBtn.setTitle("SELECT MY DEALERS", for: .normal)
            break
            
        case 1:
            sender.selectedSegmentIndex = 1
            selectDealersBtn.setTitle("BLOCK DEALERS", for: .normal)
            self.tableView.reloadData()
            break
            
        default:
            Logger.log(message: "error", event: .i)
        }
    }
    @IBOutlet weak var tableView: UITableView!
    lazy var navigationBar: NavView = {
        let header = NavView()
        header.menuBtn.setImage(UIImage(named: "arrow-back"), for: .normal)
        header.titleLbl.isHidden = true
        header.contentView.backgroundColor = .clear
        header.msgSettingBtn.isHidden = true
        header.menuBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
    }()
    
    @objc func popView() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupView() {
        navBarContainerView.addSubview(navigationBar)
        setupConstraints()
    }
    
    func setupConstraints(){
        var topConst: CGFloat = 20
        var navHeight: CGFloat = 44
        if hasTopNotch{
            topConst = 0
            navHeight *= 2
            navBarContainerHC.constant = navHeight
        }
        navigationBar.topAnchor.constraint(equalTo: navBarContainerView.topAnchor, constant: topConst).isActive = true
        navigationBar.leftAnchor.constraint(equalTo:  navBarContainerView.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo:  navBarContainerView.rightAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: navHeight).isActive = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        setupView()
    
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getData()
        blockList()

    }
}

extension MyDealersViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if CustomSegmentControl.selectedSegmentIndex == 0{
            return self.myDealers.count
        }
    
        else if CustomSegmentControl.selectedSegmentIndex == 1{
            return self.myBlockList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyDealersTableViewCell
        cell.selectionStyle = .none
        var isHidden = false
        if CustomSegmentControl.selectedSegmentIndex == 0{
            cell.dealerName.text = myDealers[indexPath.row].dealerName
            cell.dealerAddress.text = myDealers[indexPath.row].address
            cell.phoneNumber.text = myDealers[indexPath.row].mobNumber
            cell.cancel.isHidden = true
        }
        
        else if CustomSegmentControl.selectedSegmentIndex == 1{
            cell.dealerName.text = myBlockList[indexPath.row].dealerName
            isHidden = true
            cell.cancel.isHidden = false
            cell.cancel.tag = indexPath.row
            cell.cancel.addTarget(self, action: #selector(removeBlacklist), for: .touchUpInside)
            
        }
        
       
        cell.dealerAddress.isHidden = isHidden
        cell.phoneNumber.isHidden = isHidden
        return cell
    }
    
    @objc func removeBlacklist(sender: UIButton){
        ViewControllerUtils().showActivityIndicator(uiView: self.view)
        guard let dealerId = myBlockList[sender.tag].uuid, dealerId != ""
            else {
                return
        }
        
        let url = URL(string: GlobalKey.baseUrl + "/customers/unmark/blacklist")!
        let token = Helper().getValueForKey(key: GlobalKey.apiToken)
        
        let parameterDictionary: [String: Any] = ["api_token": token as Any, "dealers": [dealerId]]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: [])
            else{
                return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let task = session.dataTask(with: request) {(data, response, error) in
            guard let data = data, error == nil
                else{
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            
            DispatchQueue.main.async {
                let responseJson = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                if let jsonData = responseJson as? [String: Any]
                {
                    if let myResults = jsonData["message"] as? String {
                        let alert = UIAlertController(title: "", message: myResults, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                            action in
                            
                            self.myBlockList.remove(at: sender.tag)
                            self.tableView.reloadData()
                            
                        } ))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                ViewControllerUtils().hideActivityIndicator(uiView: self.view)
            }
           
        }
        task.resume()
    }
    
    func getData(){
        ViewControllerUtils().showActivityIndicator(uiView: self.view)
        var dealerList = Dealers()
        
        let token = Helper().getValueForKey(key: GlobalKey.apiToken)
        
        let url = URL(string: GlobalKey.baseUrl + "/customers/list/saveddealers")!
        
        let json = ["api_token": token]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: json.self, options: []) else {
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil
                else {
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            if let jsonData = responseJSON as? [String : Any]
            {
                if let myResults = jsonData["data"] as? [[String : Any]]
                {
                    self.myDealers.removeAll()
                    for value in myResults
                    {
                        if let dealerName = value["name"] as? String  {
                            dealerList.dealerName = dealerName
                        }
                        if let dealerAddress = value["address"] as? String {
                            dealerList.address = dealerAddress
                        }
                        if let mobNumber = value["mobileno"] as? String {
                            dealerList.mobNumber = mobNumber
                        }
                        if let uuid = value["uuid"] as? String {
                            self.saved.append(uuid)
                            
                        }
                        
                        self.myDealers.append(dealerList)
                    }
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                    }
                    if let myError = jsonData["status"] as? String{
                        if myError == "ERROR" {
                            if let message = jsonData["message"] as? String{
                                
                                DispatchQueue.main.async {
                                    let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                                        action in
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                    ))
                                    ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                                    self.present(alert, animated: true, completion: nil)
                                }}
                        }
                    }
                }
                
            }
            
        }
        task.resume()
    }
    
    func blockList(){
        ViewControllerUtils().showActivityIndicator(uiView: self.view)
        var blockList = Dealers()
        let token =
            Helper().getValueForKey(key: GlobalKey.apiToken)
        let url = URL(string: GlobalKey.baseUrl + "/customers/list/blacklist")!
        let json = ["api_token": token]
        guard let jsonData = try? JSONSerialization.data(withJSONObject: json.self, options: []) else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
    
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil
                else {
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            if let jsonData = responseJSON as? [String : Any]
            {
                if let myResults = jsonData["data"] as? [[String : Any]]
                {
                    self.myBlockList.removeAll()
                    for value in myResults
                    {
                        if let dealerName = value["name"] as? String  {
                           blockList.dealerName = dealerName
                        }
                        if let dealerId = value["uuid"] as? String{
                            blockList.uuid = dealerId
                        }
                
                       self.myBlockList.append(blockList)
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                    }
                }
                
             /*       if let myError = jsonData["status"] as? String{
                    if myError == "ERROR" {
                        if let message = jsonData["message"] as? String{
                            
                            DispatchQueue.main.async {
                                let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                                    action in
                                    self.navigationController?.popViewController(animated: true)
                                }
                                ))
                                 ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                                self.present(alert, animated: true, completion: nil)
                            }}
                }
                }*/
            }
        }
        task.resume()
    }
}
