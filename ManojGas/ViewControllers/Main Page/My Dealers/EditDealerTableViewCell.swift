//
//  EditDealerTableViewCell.swift
//  ManojGas
//
//  Created by Mac on 6/3/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit

class EditDealerTableViewCell: UITableViewCell {
    var dealerSelected = [String]()
    var dealers = List()
    @IBOutlet weak var checkBox: UIButton!{
            didSet{
                checkBox.setImage(UIImage(named : "remember-me-selected"), for: .selected)
                checkBox.setImage(UIImage(named : "remember-me-unselected"), for: .normal)
            }
        
    }
    @IBAction func checkBox(_ sender: UIButton) {
        
    }
    @IBOutlet weak var dealerName: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var contactNo: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
