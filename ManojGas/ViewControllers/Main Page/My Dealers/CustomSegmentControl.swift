//
//  CustomSegmentControl.swift
//  ManojGas
//
//  Created by Mac on 4/19/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit
@IBDesignable
class CustomSegmentControl: UIControl {
    
    var selector: UIView!
    var buttons = [UIButton]()
    var selectedSegmentIndex = 0
    @IBInspectable
    var borderWidth: CGFloat = 0{
        didSet{
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable
    var commaSeparatedButtonTitles: String = "" {
        didSet {
            updateViews()
        }
    }
    
    @IBInspectable
    var textColor: UIColor  = .lightGray {
        didSet{
            updateViews()
        }
    }
    
    @IBInspectable
    var selectorColor: UIColor = .red{
        didSet{
            updateViews()
        }
    }
   
    func updateViews() {
        //self.clipsToBounds = true
        
        buttons.removeAll()
        subviews.forEach { $0.removeFromSuperview() }
        
        let buttonTitles = commaSeparatedButtonTitles.components(separatedBy: ",")
        for buttonTitle in buttonTitles {
            let button = UIButton(type: .system)
            button.setTitle(buttonTitle, for: .normal)
            button.setTitleColor(textColor, for: .normal)
            button.addTarget(self, action: #selector(buttonTapped(button:)), for: .touchUpInside)
            buttons.append(button)
        }
        
        let selectorWidth = frame.width/CGFloat(buttonTitles.count)
      
        selector = UIView(frame: CGRect(x: 0, y: 34, width: selectorWidth, height: 5))
        selector.backgroundColor = selectorColor
        addSubview(selector)
        
        
        let sv = UIStackView(arrangedSubviews: buttons)
        sv.axis = .horizontal
        sv.alignment = .fill
        sv.distribution = .fillProportionally
        addSubview(sv)
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        sv.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        sv.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        sv.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
    }
    
    @objc func buttonTapped(button: UIButton) {
        for (buttonIndex, btn) in buttons.enumerated()
        {
            if btn == button {
                selectedSegmentIndex = buttonIndex
                let selectorStartPosition = frame.width/CGFloat(buttons.count) * CGFloat(buttonIndex)
                UIView.animate(withDuration: 0.3, animations: {
                    self.selector.frame.origin.x = selectorStartPosition
                    if buttonIndex == 0{
                        self.selector.frame.origin.x = 0
                    }
                })
            }
        }
        sendActions(for: .valueChanged)
    }
    
    override func draw(_ rect: CGRect) {
     //   layer.cornerRadius = 12
        
    }
    
    
}
