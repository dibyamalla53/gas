//
//  MyDealersTableViewCell.swift
//  ManojGas
//
//  Created by Mac on 4/23/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit

class MyDealersTableViewCell: UITableViewCell {

    @IBOutlet weak var dealerName: UILabel!
    @IBOutlet weak var dealerAddress: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var cancel: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
