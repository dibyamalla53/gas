//
//  EditDealerViewController.swift
//  ManojGas
//
//  Created by Mac on 6/3/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit

class EditDealerViewController: UIViewController {
    var dealerUuid = [String]()
    var testDealerUUID  = [String: String]()
    var identifier = 100
    var url: URL!
    var asa = String()
    @IBOutlet weak var navBarContainerView: UIView!
    @IBOutlet weak var navBarContainerHC: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var saveBtn: UIButton!{
        didSet{
            let customerGrad = setGradient(topColor: UIColor(hexString: Color.CustomerButton.topGrad), bottomColor:UIColor(hexString: Color.CustomerButton.bottomGrad), frame:CGRect(x: 0, y: 0, width: screenSize.width-40, height: 40), cornerRadius: 20)
            
            saveBtn.layer.insertSublayer(customerGrad, below: saveBtn.titleLabel?.layer)
            saveBtn.setTitleColor(.white, for: .normal)
            
        }
    }
    
    @IBAction func saveBtn(_ sender: UIButton) {
        
        if identifier == 0{
             self.url = URL(string: GlobalKey.baseUrl + "/customers/save/dealers")!
        }
        else if identifier == 1{
            self.url = URL(string: GlobalKey.baseUrl + "/customers/mark/blacklist")!
        }
        Logger.log(message: "url:\(String(describing: url))", event: .i)
        let token = Helper().getValueForKey(key: GlobalKey.apiToken)!
        let parameterDictionary: [String: Any] = ["api_token": token, "dealers": self.testDealerUUID]
        Logger.log(message: "uid:\(self.testDealerUUID)", event: .i)
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: [])
            else{
                return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil
                else{
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            
            DispatchQueue.main.async {
            
                self.dealerList()
                let responseJson = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                if let jsonData = responseJson as? [String: Any]
                {
                    if let myResults = jsonData["message"] as? String {
                        let alert = UIAlertController(title: "", message: myResults, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                            action in
                            if let status = jsonData["status"] as? String {
                                if status != "ERROR" {
                                     self.navigationController?.popViewController(animated: true)
                                }
                            }
                        } ))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
            }
        }
        task.resume()
        
    }
    
    var dealers = List()
    var allDealers = [List]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        setupView()
        dealerList()
    }

    lazy var navigationBar: NavView = {
        let header = NavView()
        header.menuBtn.setImage(UIImage(named: "arrow-back"), for: .normal)
        header.titleLbl.isHidden = true
        header.contentView.backgroundColor = .clear
        header.msgSettingBtn.isHidden = true
        header.menuBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
    }()
   
    @objc func popView() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func setupView() {
        navBarContainerView.addSubview(navigationBar)
        setupConstraints()
    }
    
    func setupConstraints(){
        var topConst: CGFloat = 20
        var navHeight: CGFloat = 44
        if hasTopNotch{
            topConst = 0
            navHeight *= 2
            navBarContainerHC.constant = navHeight
        }
        navigationBar.topAnchor.constraint(equalTo: navBarContainerView.topAnchor, constant: topConst).isActive = true
        navigationBar.leftAnchor.constraint(equalTo:  navBarContainerView.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo:  navBarContainerView.rightAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: navHeight).isActive = true
        
    }
}

extension EditDealerViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allDealers.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! EditDealerTableViewCell
        cell.dealerName.text = allDealers[indexPath.row].name
        cell.address.text = allDealers[indexPath.row].address
        cell.contactNo.text = allDealers[indexPath.row].contact
        cell.checkBox.tag = indexPath.row
        if let btnChk = cell.checkBox  {
            btnChk.addTarget(self, action: #selector(checkboxClicked(_ :)), for: .touchUpInside)
        }
        if identifier == 0{
            if allDealers[indexPath.row].saved == "True"{
                cell.checkBox.isSelected = true
                testDealerUUID["\(indexPath.row)"] = allDealers[indexPath.row].uuid
                print(testDealerUUID)
            }}
        
        cell.selectionStyle = .none
        return cell
    }
   
    @objc func checkboxClicked(_ sender: UIButton) {

        sender.isSelected = !sender.isSelected
            if sender.isSelected == true {
//            dealerUuid.append(self.allDealers[sender.tag].uuid!)
       //     print(dealerUuid)

            testDealerUUID["\(sender.tag)"] = self.allDealers[sender.tag].uuid!
                print(testDealerUUID)
            
        }
            else{
                
                testDealerUUID.removeValue(forKey: "\(sender.tag)")
                print("removed: \(testDealerUUID)")
                
//                if let uid = self.allDealers[sender.tag].uuid {
//                    if !dealerUuid.isEmpty {
//                        for (index, value) in dealerUuid.enumerated(){
//             //               print("index: \(index), value: \(value)")
//                            if uid == value {
//                                dealerUuid.remove(at: index)
//                            }
//                        }
//                    }
//                }
//
//
//            print(dealerUuid)
    }
    }

    func dealerList(){
        
        let url = URL(string: GlobalKey.baseUrl + "/dealers/all")
        let token = Helper().getValueForKey(key: GlobalKey.apiToken)
        
        let parameterDictionary = ["api_token": token]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: [])
            else{
                return
        }
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) {(data, response, error) in
            guard let data = data, error == nil
                else{
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            
            let responseJson = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            
            if let jsonData = responseJson as? [String: Any]
            {
                
                if let myResults = jsonData["data"] as? [[String: Any]] {
                    self.allDealers.removeAll()
                    for value in myResults
                    {
                        
                        if let dealerName = value["name"] as? String{
                          self.dealers.name = dealerName
                        }
                        
                        if let dealerAddress = value["address"] as? String{
                            self.dealers.address = dealerAddress
                        }
                        
                        if let contactNo = value["mobileno"] as? String{
                            self.dealers.contact = contactNo
                        }
                        if let uid = value["uuid"] as? String{
                            self.dealers.uuid = uid
                        }
                        self.dealers.saved = value["saved"] as? String
                        
                        self.allDealers.append(self.dealers)
                    }
                  
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        
                    }
                }
                
                if let myError = jsonData["status"] as? String{
                    if myError == "ERROR" {
                        if let message = jsonData["message"] as? String{
                            let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                                action in
                                self.navigationController?.popViewController(animated: true)
                            }
                            ))
                            
                            self.present(alert, animated: true, completion: nil)
                        }}
                }
            }
        }
        task.resume()
    }


}
