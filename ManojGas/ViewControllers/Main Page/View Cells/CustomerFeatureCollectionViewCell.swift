//
//  CustomerFeatureCollectionViewCell.swift
//  ManojGas
//
//  Created by f1soft on 11/14/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import UIKit

class CustomerFeatureCollectionViewCell: UICollectionViewCell {
    let containerView: RoundedView = {
        let view = RoundedView()
        view.backgroundColor = .white
        view.cornerRadius = 6
        view.shadowOpacity = 0.08
        view.shadowRadius = 3
        view.shadowOffset = CGSize(width: 1, height: 2)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let imageView: UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = UIView.ContentMode.scaleAspectFit
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }()
    
    let titleLbl: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: Font.MyriadProRegular, size: Font.fontSize14)
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    var model: CustomerDashboardFeatureModel? {
        didSet{
            if let model = model {
                self.bindModel(image: model.image, title: model.title)
            }
        }
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configureViews()
        self.setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func bindModel(image: UIImage, title: NSMutableAttributedString) {
        self.imageView.image = image
        self.titleLbl.attributedText = title
    }
}

extension CustomerFeatureCollectionViewCell {
    private func configureViews() {
        addSubview(containerView)
        addSubview(imageView)
        addSubview(titleLbl)
        
    }
    
    private func setupConstraints() {
        containerView.leftAnchor.constraint(equalTo: leftAnchor, constant:0.0).isActive = true
        containerView.topAnchor.constraint(equalTo: topAnchor, constant:0.0).isActive = true
        containerView.rightAnchor.constraint(equalTo: rightAnchor, constant:0.0).isActive = true
        containerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant:0.0).isActive = true
        
        imageView.topAnchor.constraint(equalTo:  containerView.topAnchor, constant:14.0).isActive = true
        imageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor, constant:0.0).isActive = true
        imageView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.5, constant: 0).isActive = true
        imageView.heightAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.5, constant: 0).isActive = true
        
        titleLbl.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 8.0).isActive = true
        titleLbl.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 4.0).isActive = true
        titleLbl.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -4.0).isActive = true
        titleLbl.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: -8.0).isActive = true
        
        
    }
}
