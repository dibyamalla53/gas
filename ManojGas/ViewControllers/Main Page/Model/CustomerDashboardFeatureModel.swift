//
//  CustomerDashboardFeatureModel.swift
//  ManojGas
//
//  Created by f1soft on 11/14/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import Foundation
import UIKit

struct CustomerDashboardFeatureModel {
    let image: UIImage
    let title: NSMutableAttributedString
}
