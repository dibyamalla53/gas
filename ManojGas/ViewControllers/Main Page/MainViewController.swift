//
//  MainViewController.swift
//  ManojGas
//
//  Created by f1soft on 11/12/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import UIKit

// 7.Struct for add storyboards which you want show on navigation drawer
struct DrawerArray {
    static let array = ["SettingViewController", "SafetyTipViewController", "FeedbackViewController","ShareAppViewController", "RateUsViewController","ContactUsViewController", "AboutUsViewController", "LogoutViewController"]
}

class MainViewController: UIViewController, DrawerControllerDelegate, SettingViewControllerDelegate {
    
    var drawerView = DrawerView()
    var messageView = messageSetting()
    var contactView = ContactUsViewController()
    
    @IBOutlet weak var bottomImage: UIImageView!

    func pushTo(viewController: String) {
        Logger.log(message: "view controlelr: \(viewController)", event: .i)
        if viewController == "SettingViewController"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
        else if viewController == "LogoutViewController" {
            appDelegate.setUserViewControllerAsRoot(usingAnimation: .present)
            UserDefaults.standard.removeObject(forKey: "api_token")
            
        }
        else if viewController == "FeedbackViewController" {
            let vc=self.storyboard?.instantiateViewController(withIdentifier: "FeedbackViewController")as! FeedbackViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
        else if viewController == "ContactUsViewController" {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if viewController == "AboutUsViewController" {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as!
            AboutUsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if viewController == "ShareAppViewController" {
            
            let activityViewController = UIActivityViewController(activityItems: ["www.google.com"], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = ( drawerView )
            activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 200, y: 400, width: 0, height:0)
            self.present(activityViewController, animated: true, completion: nil)
            
        }
        else if viewController == "SafetyTipViewController"{
            let vc = storyBoard.instantiateViewController(withIdentifier: "SafetyTipsViewController") as! SafetyTipsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if viewController == "RateUsViewController" {}
    }
    
    func okBtnTapped() {
        Logger.log(message: "reload view", event: .i)
        self.changeLang()
    }
  
    
    var notification: [AnyHashable: Any]? {
        didSet {
            Logger.log(message: "for notification: \(String(describing: notification))", event:  .i)
        }
    }
    
    lazy var navigationBar: NavView = {
        let header = NavView()
        header.menuBtn.setImage(UIImage(named: "main-page-menu"), for: .normal)
        header.menuBtn.addTarget(self, action: #selector(openSideMenu), for: .touchUpInside)
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
    }()
    
    lazy var dealerView: DealerView = {
        let view = DealerView()
        view.navigationController = self.navigationController
        view.viewController = self
        view.dealerNameLbl.text = Helper().getValueForKey(key: GlobalKey.customername)
        view.requestCountLbl.text = "You have 0 Customer Request."
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var customerView: CustomerView = {
        let view = CustomerView()
        view.delegate=self
        view.customerNameLbl.text = Helper().getValueForKey(key: GlobalKey.customername)
        view.navigationController = self.navigationController
        view.viewController = self
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    var user: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(hexString: Color.backgroundColor)
        changeLang()
        if let u = Helper().getValueForKey(key: Key.user) {
            switch u {
            case UserType.customer.rawValue:
                setupCustomerView()
                navigationBar.msgSettingBtn.addTarget(self, action: #selector(userMessageSetting), for: .touchUpInside)
                
            case UserType.dealer.rawValue:
                setupDealerView()
                navigationBar.msgSettingBtn.isHidden = true
                
            default:
                Logger.log(message: "user not logged in as", event: .i)
            }
        }
    }
    
    @objc func userMessageSetting(){
        
        messageView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(messageView)
        messageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        messageView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        messageView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        messageView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant:0).isActive = true
       
    }
    
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        setupCustomerView()
        
    }
    
    @IBAction func disAppear(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func changeLang(){
        let greetingsMessage = NSLocalizedString("main-page-nav-title", comment: "Manoj Gas")
        navigationBar.titleLbl.text = greetingsMessage
    }
    
    @objc func openSideMenu() {
        drawerView = DrawerView(aryControllers: DrawerArray.array, controller:self)
        drawerView.delegate = self
        drawerView.show()
    }
    
}



extension MainViewController  {
    
    //MARK:- dealer view layout
    private func setupDealerView() {
        view.addSubview(navigationBar)
        view.addSubview(dealerView)
        setupConstraints()
        setupDealerContstraints()
    }
    
    private func setupConstraints() {
        var topConst: CGFloat = 20
        var navHeight: CGFloat = 44
        if hasTopNotch {
            topConst = 0
            navHeight *= 2
        }
        navigationBar.topAnchor.constraint(equalTo: view.topAnchor, constant:topConst).isActive = true
        navigationBar.leftAnchor.constraint(equalTo:  view.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo:  view.rightAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: navHeight).isActive = true
    }
    
    private func setupDealerContstraints() {
        dealerView.topAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant:0).isActive = true
        dealerView.leftAnchor.constraint(equalTo:  view.leftAnchor).isActive = true
        dealerView.rightAnchor.constraint(equalTo:  view.rightAnchor).isActive = true
        dealerView.bottomAnchor.constraint(equalTo:  view.bottomAnchor).isActive = true
    }
    
    //MARK:- customer view layout
    private func setupCustomerView() {
        view.addSubview(navigationBar)
        view.addSubview(customerView)
        setupConstraints()
        setupCustomerContstraints()
        
    }
    
    private func setupCustomerContstraints() {
        customerView.topAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant:0).isActive = true
        customerView.leftAnchor.constraint(equalTo:  view.leftAnchor).isActive = true
        customerView.rightAnchor.constraint(equalTo:  view.rightAnchor).isActive = true
        customerView.bottomAnchor.constraint(equalTo:  view.bottomAnchor).isActive = true
        
    }
}


extension MainViewController:TabDelegate{
    func didButtonTapped() {
        
        let vc = storyBoard.instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
      
        
    }
}
extension MainViewController: PopViewDelegate {
    func favButtonTapped() {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteDealersViewController") as! FavouriteDealersViewController

            self.navigationController?.pushViewController(vc, animated: true)
    }

}
