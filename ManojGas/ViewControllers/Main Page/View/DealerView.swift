//
//  DealerView.swift
//  ManojGas
//
//  Created by f1soft on 11/13/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import UIKit
import DropDown

class DealerView: UIView {
    var navigationController: UINavigationController?
    var viewController: UIViewController?
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var dealerNameLbl: UILabel!
    @IBOutlet weak var requestCountLbl: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var gasStatusBtn: UIButton!
    @IBOutlet weak var gasStatusLbl: UILabel!
    @IBOutlet weak var gasStatusImgView: UIImageView!
    @IBOutlet weak var dropDownBgView: UIView!
    @IBOutlet weak var bottomImage: UIImageView!
    @IBOutlet weak var newOderBgView: UIView!{
        didSet{
            Logger.log(message: "frame: \(newOderBgView.bounds)", event: .i)
             let gradientView = setGradient(topColor: UIColor(hexString: Color.CustomerButton.topGrad), bottomColor:UIColor(hexString: Color.CustomerButton.bottomGrad), frame:CGRect(x: 0, y: 0, width: screenSize.width, height: 75))
            newOderBgView.layer.insertSublayer(gradientView, at: 0)
        }
    }
    @IBOutlet weak var newOrderCountLbl: UILabel!
    @IBOutlet weak var whiteGasImgView: UIImageView!{
        didSet{
            whiteGasImgView.tintColor = .white
        }
    }
    var gasUpdate: Int?
    
    func newCount(){
        let url = URL(string: GlobalKey.baseUrl + "/dealers/get/newordercount")!
        let api_token = Helper().getValueForKey(key: GlobalKey.apiToken)
        let parameterDictionary = ["api_token": api_token]
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: []) else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil
                else {
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            if let jsonData = responseJSON as? [String : Any]
            {
                 DispatchQueue.main.async {
                        let data = jsonData["data"] as! Int
                        self.newOrderCountLbl.text = "\(data) \n New Orders"
                        Logger.log(message: "\(data)", event: .i)
                    }
                
            }
        }
        
        task.resume()
        
    }
    
    func notification(){
        let url = URL(string: GlobalKey.baseUrl + "/dealers/get/newordercount")!
        let api_token = Helper().getValueForKey(key: GlobalKey.apiToken)
        let parameterDictionary = ["api_token": api_token]
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: []) else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil
                else {
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            if let jsonData = responseJSON as? [String : Any]
            {
                DispatchQueue.main.async {
                    let data = jsonData["data"] as! Int
                    self.requestCountLbl.text = "You have \(data) Customer Request"
                    Logger.log(message: "\(data)", event: .i)
                }
                
            }
        }
        
        task.resume()
    }
    
    var gasAvailableStatus = true
    
    let gasStatusDropDown = DropDown()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        newCount()
        notification()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
        newCount()
        notification()
    }
    
    private func setupViews() {
       UINib(nibName: "DealerView", bundle: nil).instantiate(withOwner: self, options: nil)
        addSubview(contentView)
        contentView.backgroundColor = .white
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        setUpGasStatusDropDown()
        setupConstraints()
        
        
    }
    
    private func setupConstraints() {
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        contentView.widthAnchor.constraint(equalToConstant: screenSize.width).isActive = true
    }
    
    
    
    private func setUpGasStatusDropDown(){
        gasStatusDropDown.anchorView = self.dropDownBgView
        gasStatusDropDown.direction = .bottom
        gasStatusDropDown.dismissMode = .onTap
        let dataSource = ["Gas available","Gas unavailable"]
        gasStatusDropDown.dataSource = dataSource
        gasStatusDropDown.bottomOffset = CGPoint(x: 0, y:(gasStatusDropDown.anchorView?.plainView.bounds.height)!)
        gasStatusDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            Logger.log(message: "gas stat selected:\(index), item: \(item)", event:.i)
            
            if dataSource[0] == item {
                self.gasStatusImgView.tintColor = UIColor(hexString: Color.CustomerButton.topGrad)
                self.gasAvailableStatus = true
                self.gasUpdate = 1
                self.gasAvailable()
               
            }else {
                
                self.gasStatusImgView.tintColor = UIColor(hexString: Color.DealerButton.bottomGrad)
                self.gasAvailableStatus = false
                self.gasUpdate = 0
                self.gasAvailable()
            }
            self.gasStatusLbl.text = item
        }
    }
    
    func gasAvailable(){
        let url = URL(string: GlobalKey.baseUrl + "/dealers/update/gasavailability")!
        let token = Helper().getValueForKey(key: GlobalKey.apiToken)!
        let gasAvail = self.gasUpdate
        Logger.log(message: "gasAvail: \(String(describing: gasAvail))", event: .i)
        let parameterDictionary: [String: Any] = ["api_token": token,
                                                  "gas_availability": gasAvail as Any,
                                                  ]
        
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: [])
            else{
                return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) {(data, response, error) in
            guard let data = data, error == nil
                else{
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            let responseJson = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            if let data = responseJson as? [String: Any]{
               
                    if let message = data["message"] as? String{
                        Logger.log(message: "message:\(message)", event: .i)
                    }
            }
        }
        task.resume()
    }
    
    
    @IBAction func editAction(_ sender: UIButton) {
        Logger.log(message: "edit action", event: .i)
        let vc = storyBoard.instantiateViewController(withIdentifier: "EditViewController") as! EditViewController
        vc.newName = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func gasAction(_ sender: UIButton) {
        gasStatusDropDown.show()
    }
   
    
    @IBAction func rateAppAction(_ sender: UIButton) {
        Logger.log(message: "rateAppAction", event: .i)
    }
    @IBAction func shareAppAction(_ sender: UIButton) {
        Logger.log(message: "shareAppAction", event: .i)
        
        if let parent = self.viewController {
            let activityVC = UIActivityViewController(activityItems: ["www.google.com"], applicationActivities: nil)
            activityVC.popoverPresentationController?.sourceView = (sender)
            activityVC.popoverPresentationController?.sourceRect = CGRect(x: 200, y:200, width: 0, height:0)
            parent.present(activityVC, animated: true, completion: nil)
            Logger.log(message: "parenet view controller: \(parent)", event: .i)

            
        }
    }
    @IBAction func newOrderButton(_ sender: UIButton) {
        Logger.log(message: "New Order pressed", event: .i)
        
        let vc = storyBoard.instantiateViewController(withIdentifier: "NewOrderViewController") as! NewOrderViewController
        self.navigationController?.pushViewController(vc, animated: true)

    }
    @IBAction func orderHistoryButton(_ sender: UIButton) {
        Logger.log(message: "order history pressed", event: .i)
        
        let vc = storyBoard.instantiateViewController(withIdentifier: "OrderViewController") as! OrderViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension DealerView: NameChagedDelegate{
    func updateName(name: String) {
        dealerNameLbl.text = name
    }
}
