//
//  CustomerView.swift
//  ManojGas
//
//  Created by f1soft on 11/13/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import UIKit

class CustomerView: UIView{
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var customerNameLbl: UILabel!
    @IBOutlet weak var responseCountLbl: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    var delegate:TabDelegate?
    @IBOutlet weak var orderGasBtn: UILabel!
    @IBOutlet weak var bottomImage: UIImageView!
    @IBOutlet weak var locate: UILabel!
    
    @IBAction func editBtn(_ sender: UIButton) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "EditViewController") as! EditViewController
        vc.newName = self
        self.navigationController?.pushViewController(vc, animated: true)
        Logger.log(message: "edit action", event: .i)
    }
    
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet {
            
            
        }
    }
    
    @IBOutlet weak var orderGasTitleLbl: UILabel!{
        didSet{
            orderGasTitleLbl.multipleColorText(text: "ORDER GAS", range: NSRange(location: 6, length: 3), color: .white, font: UIFont(name: Font.MyriadProLight, size: Font.fontSize15))
        }
    }
    @IBOutlet weak var locateDistriTitleLbl: UILabel!{
        didSet{
            locateDistriTitleLbl.multipleColorText(text: "LOCATE DISTRIBUTOR", range: NSRange(location: 7, length: 11), color: .white, font: UIFont(name: Font.MyriadProLight, size: Font.fontSize15))
        }
    }
    
    let categoryMenus = [["image":"logo-gas", "title":"Services"],["image":"logo-gas", "title":"Services"],["image":"logo-gas", "title":"Services"]]
    let viewModel = CustomerDashboardFeatureViewModel()
    var navigationController:UINavigationController?
    var viewController: UIViewController?
    
    func notification(){
        let url = URL(string: GlobalKey.baseUrl + "/customers/get/newordercount")!
        let api_token = Helper().getValueForKey(key: GlobalKey.apiToken)
        let parameterDictionary = ["api_token": api_token]
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: []) else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil
                else {
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            if let jsonData = responseJSON as? [String : Any]
            {
                DispatchQueue.main.async {
                    let data = jsonData["data"] as! Int
                    self.responseCountLbl.text = "You have \(data) Dealer Response"
                    Logger.log(message: "\(data)", event: .i)
                }
                
            }
        }
        
        task.resume()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        notification()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
        notification()
    }
    
    private func setupViews() {
        UINib(nibName: "CustomerView", bundle: nil).instantiate(withOwner: self, options: nil)
        addSubview(contentView)
        contentView.backgroundColor = .white
        contentView.translatesAutoresizingMaskIntoConstraints = false
        setupConstraints()
        self.configureCollectionView()
        //collectionView.reloadData()
    }
    
    private func configureCollectionView(){
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumInteritemSpacing  = 4.0
        flowLayout.minimumLineSpacing       = 8.0
        flowLayout.scrollDirection = .vertical
        //collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.backgroundColor = .white
        collectionView.setCollectionViewLayout(flowLayout, animated: true)
        collectionView.contentInset = UIEdgeInsets(top: 8.0, left: 8.0, bottom: 8.0, right: 8.0)
        collectionView.alwaysBounceVertical = false
        collectionView.alwaysBounceHorizontal = false
        collectionView.allowsSelection = true
        collectionView.isScrollEnabled = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(CustomerFeatureCollectionViewCell.self, forCellWithReuseIdentifier: "CustomerFeatureCollectionViewCell")
    }
    
    private func setupConstraints() {
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        contentView.widthAnchor.constraint(equalToConstant: screenSize.width).isActive = true
    }

    @IBAction func locateDealer(_ sender: UIButton) {
       let vc = storyBoard.instantiateViewController(withIdentifier: "LocateDistributorViewController") as! LocateDistributorViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func orderGasBtnnow(_sender:UIButton){
        
            delegate?.didButtonTapped()
    }
    

}
extension CustomerView: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        Logger.log(message: "count: \(viewModel.menus.count)", event: .i)
        return viewModel.menus.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let categoryCell = collectionView.dequeueReusableCell(withReuseIdentifier:"CustomerFeatureCollectionViewCell", for: indexPath) as! CustomerFeatureCollectionViewCell
       categoryCell.model = viewModel.menus[indexPath.row]
        return categoryCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.size.width-collectionView.contentInset.left-collectionView.contentInset.right-(3*4))/3
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Logger.log(message: "did select item at index path: \(indexPath.row)", event: .i)
        
        if indexPath.row == 0 {
            let vc = storyBoard.instantiateViewController(withIdentifier: "OrderHistoryViewController") as! OrderHistoryViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
            
        else if indexPath.row == 1{
                let vc = storyBoard.instantiateViewController(withIdentifier: "MyDealersViewController")
            as! MyDealersViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
            
            
        else if indexPath.row == 2{
            
            let vc = storyBoard.instantiateViewController(withIdentifier: "SafetyTipsViewController") as! SafetyTipsViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
            
        else if indexPath.row == 3 {
           
            if let parent = self.viewController {
                let activityVC = UIActivityViewController(activityItems: ["www.google.com"], applicationActivities: nil)
                activityVC.popoverPresentationController?.sourceView = ( collectionView )
                activityVC.popoverPresentationController?.sourceRect = CGRect(x: 200, y: 400, width: 0, height:0)
                parent.present(activityVC, animated: true, completion: nil)
                Logger.log(message: "parenet view controller: \(parent)", event: .i)
                
                
            }
        }
        
        
    }
}

protocol TabDelegate {
    
    func didButtonTapped()
}


 class CustOrderGradView: UIView {
    var delegate: TabDelegate?
    override func draw(_ rect: CGRect) {
        

        let grad = setGradient(topColor: UIColor(hexString: Color.DealerButton.topGrad), bottomColor: UIColor(hexString: Color.DealerButton.bottomGrad), frame: rect)
        self.layer.insertSublayer(grad, at: 0)
        self.roundedLeftCorner()
    }
}
 
    class CustLocateGradView: UIView {
        override func draw(_ rect: CGRect) {
            let grad = setGradient(topColor: UIColor(hexString: Color.CustomerButton.topGrad), bottomColor: UIColor(hexString: Color.CustomerButton.bottomGrad), frame: rect)
            self.layer.insertSublayer(grad, at: 0)
            self.roundedLeftCorner()
        }
    }
   
  /*  func didButtonTapped() {
    //  let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
        
        
      self.presentViewController(vc, animated: true, completion: nil)
    }
}*/

extension CustomerView: NameChagedDelegate{
    func updateName(name: String) {
        customerNameLbl.text = name
    }
}


    


