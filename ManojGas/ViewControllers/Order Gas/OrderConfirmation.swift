//
//  OrderConfirmation.swift
//  ManojGas
//
//  Created by Mac on 5/27/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit

class OrderConfirmation: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var textField: UITextField!{
        didSet{
            self.textField.text = "Are you sure to Order Gas?"
        }
    }
    @IBOutlet weak var Yes: UIButton!
    @IBOutlet weak var No: UIButton!
    @IBAction func Yes(_ sender: Any) {
    }
    @IBAction func No(_ sender: Any) {
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    func setupViews() {
        Bundle.main.loadNibNamed("OrderConfirmation", owner: self, options: nil)
        addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        contentView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant:0).isActive = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        contentView.addGestureRecognizer(tapGesture)
        
    }
    @objc func handleTap(sender: UITapGestureRecognizer) {
        self.removeFromSuperview()

}
}
