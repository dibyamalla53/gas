//
//  AllDealerTableViewCell.swift
//  ManojGas
//
//  Created by Mac on 5/23/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit

class TestBtn: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        Logger.log(message: "\(self.frame)", event: .i)
        let orderButton = setGradient(topColor: UIColor(hexString: Color.DealerButton.topGrad), bottomColor:UIColor(hexString: Color.DealerButton.bottomGrad), frame:CGRect(x: 0, y: 0, width: 90, height: 30))
        
//        TestBtn.layer.insertSublayer(orderButton, below: TestBtn.titleLabel?.layer)
//        TestBtn.setTitle("Order Gas", for: .normal)
//        TestBtn.roundedCorners()
    }
    override func draw(_ rect: CGRect) {
        print(rect)
    }
}

class AllDealerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dealerImage: UIImageView!
    @IBOutlet weak var gasStatus: UILabel!
    @IBOutlet weak var dealerName: UILabel!
    @IBOutlet weak var dealerAddress: UILabel!
    @IBOutlet weak var contactNo: UILabel!
    @IBOutlet weak var orderBtn: UIButton!{
        didSet{
            let orderButton = setGradient(topColor: UIColor(hexString: Color.DealerButton.topGrad), bottomColor:UIColor(hexString: Color.DealerButton.bottomGrad), frame:CGRect(x: 0, y: 0, width: 90, height: 30))
            orderBtn.setTitle("Order Gas", for: .normal)
            orderBtn.roundedCorners()
             orderBtn.layer.insertSublayer(orderButton, below: orderBtn.titleLabel?.layer)
        }
    }
    @IBOutlet weak var gasImage: UIImageView!
    @IBAction func orderBtn(_ sender: Any) {

    }
    
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization cod


    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    

}
