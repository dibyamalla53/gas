//
//  OrderConfirmationViewController.swift
//  ManojGas
//
//  Created by Mac on 5/27/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit

class OrderConfirmationViewController: UIViewController {

    var dealerUuid = ""
    @IBOutlet weak var popUp: UIView!
    @IBOutlet weak var yes: UIButton!
    @IBOutlet weak var no: UIButton!
    @IBAction func yes(_ sender: UIButton) {
        ViewControllerUtils().showActivityIndicator(uiView: self.view)
        let url = URL(string: GlobalKey.baseUrl + "/customers/order/single")!
        let token = Helper().getValueForKey(key: GlobalKey.apiToken)
       
        Logger.log(message: "dealerUuid:\(dealerUuid)", event: .i)
        let parameterDictionary: [String: Any] = ["api_token" : token!, "quantity": "1", "dealer": dealerUuid as Any, "order_status": "request"]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: [])
            else{
                return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil
                else{
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            DispatchQueue.main.async {
                let responseJson = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                if let jsonData = responseJson as? [String: Any]
                {
                    
                    if let myResults = jsonData["message"] as? String {
                        let alert = UIAlertController(title: "", message: myResults, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                            action in
                            self.dismiss(animated: true, completion: nil)
                        } ))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                ViewControllerUtils().hideActivityIndicator(uiView: self.view)
            }
        }
        task.resume()
        
    }
    @IBAction func no(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
  
    @IBOutlet weak var confirm: UILabel!
    {
        didSet{
            self.confirm.text = "Are you sure to order gas?"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
     //   popUp.layer.cornerRadius = 10
        popUp.layer.borderColor = UIColor.lightGray.cgColor
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        view.addGestureRecognizer(tapGesture)
    }
    @objc func handleTap(sender: UITapGestureRecognizer) {
     self.dismiss(animated: true, completion: nil)
        
    }


}
