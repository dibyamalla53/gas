//
//  PopUpViewController.swift
//  ManojGas
//
//  Created by Mac on 12/4/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import UIKit

protocol PopViewDelegate {
    func favButtonTapped()
}

class PopUpViewController: UIViewController{
    
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    @IBAction func tapGesture(_ sender: Any) {
        print("view Tapped")
        self.dismiss(animated: true, completion: nil)
    }
    
    var delegate: PopViewDelegate?
    
    @IBOutlet weak var popView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        popView.layer.cornerRadius=10
        lineView.backgroundColor=UIColor.black
        popView.layer.borderWidth = 0.5
        popView.layer.borderColor = UIColor.lightGray.cgColor
    
        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var favoriteBtn: UIButton!{
        
        didSet{
            let buttonGrad = setGradient(topColor: UIColor(hexString: Color.CustomerButton.topGrad), bottomColor: UIColor(hexString: Color.CustomerButton.bottomGrad), frame: CGRect(x: 0, y: 0, width: screenSize.width-161, height: 61))
            
            favoriteBtn.layer.insertSublayer(buttonGrad,below : favoriteBtn.titleLabel?.layer)
        }
    }
    @IBOutlet weak var sendtoAllBtn: UIButton!{
        
        didSet{
            let buttonGrad=setGradient(topColor: UIColor(hexString: Color.CustomerButton.topGrad), bottomColor: UIColor(hexString: Color.CustomerButton.bottomGrad), frame: CGRect(x: 0, y: 0, width: screenSize.width-161, height: 61))
            
            sendtoAllBtn.layer.insertSublayer(buttonGrad,below : sendtoAllBtn.titleLabel?.layer)
        }
    }
    
    @IBAction func favouriteBtn(_ sender: UIButton) {
    
        self.dismiss(animated: true, completion: nil)

        self.delegate?.favButtonTapped()
    }
    @IBAction func sendAll(_ sender: UIButton) {
        ViewControllerUtils().showActivityIndicator(uiView: self.view)
        let url = URL(string: GlobalKey.baseUrl + "/customers/order")!
        
        let token = Helper().getValueForKey(key: GlobalKey.apiToken)
        let parameterDictionary: [String: Any] = ["api_token": token!, "quantity": 1, "order_status": "request"]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: [])
            else{
                return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) {(data, response, error) in
            guard let data = data, error == nil
                else{
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            let responseJson = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            if let data = responseJson as? [String: Any]{
                    DispatchQueue.main.async {
                        if let message = data["message"] as? String{
                            let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                                action in
                                self.dismiss(animated: true, completion: nil)
                            }
                         ))
                            
                            self.present(alert, animated: true, completion: nil)
                        }
                        ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                    }
                
            }
            
        }
        task.resume()
    
    }
}
