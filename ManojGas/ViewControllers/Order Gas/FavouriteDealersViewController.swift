//
//  FavouriteDealersViewController.swift
//  ManojGas
//
//  Created by Mac on 4/24/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit

class FavouriteDealersViewController: UIViewController {

    @IBOutlet weak var navBarContainerView: UIView!
    @IBOutlet weak var navBarContainerHC: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    var dealers = List()
    var allDealers = [List]()
    
    lazy var navigationBar: NavView = {
        let header = NavView()
        header.menuBtn.setImage(UIImage(named: "arrow-back"), for: .normal)
        header.titleLbl.isHidden = true
        header.msgSettingBtn.isHidden = true
        header.contentView.backgroundColor = .clear
        header.menuBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
    }()
    
    @objc func popView() {
       
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupView() {
        navBarContainerView.addSubview(navigationBar)
        setupConstraints()
    }

    func setupConstraints(){
        var topConst: CGFloat = 20
        var navHeight: CGFloat = 44
        if hasTopNotch{
            topConst = 0
            navHeight *= 2
            navBarContainerHC.constant = navHeight
        }
        navigationBar.topAnchor.constraint(equalTo: navBarContainerView.topAnchor, constant: topConst).isActive = true
        navigationBar.leftAnchor.constraint(equalTo:  navBarContainerView.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo:  navBarContainerView.rightAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: navHeight).isActive = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        dealerList()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
}

extension FavouriteDealersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allDealers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AllDealerTableViewCell
      
        cell.gasStatus.text = "Gas" + " " + allDealers[indexPath.row].gasAvailable!
        cell.dealerName.text = allDealers[indexPath.row].name
        cell.dealerAddress.text = allDealers[indexPath.row].address
        cell.contactNo.text = allDealers[indexPath.row].contact
        
        if allDealers[indexPath.row].gasAvailable == "Unavailable" {
            cell.orderBtn.isHidden = true
            cell.dealerImage.image = UIImage(named: "gas-unavailable")
            cell.gasImage.image = UIImage(named: "gas-red")
        }
        cell.orderBtn.setTitleColor(.white, for: .normal)
        cell.orderBtn.tag = indexPath.row
        cell.orderBtn.addTarget(self, action: #selector(order), for: .touchUpInside)
        
        return cell
    }
    
    @objc func order(sender: UIButton) {
    
    guard let dealerUuid = allDealers[sender.tag].uuid, dealerUuid != ""
        else{
            return
        }
     
      let vc =  self.storyboard?.instantiateViewController(withIdentifier: "OrderConfirmationViewController") as! OrderConfirmationViewController
        print("Button tag: \(dealerUuid)")
        vc.dealerUuid = dealerUuid
        self.present(vc, animated: true, completion: nil)
    }
    
    func dealerList(){
        ViewControllerUtils().showActivityIndicator(uiView: self.view)
        let url = URL(string: GlobalKey.baseUrl + "/dealers/all")
        let token = Helper().getValueForKey(key: GlobalKey.apiToken)
        
        let parameterDictionary = ["api_token": token]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: [])
            else{
                return
        }
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        let session = URLSession.shared
        let task = session.dataTask(with: request) {(data, response, error) in
            guard let data = data, error == nil
                else{
                    print(error?.localizedDescription ?? "No data")
                    return
            }
            
            let responseJson = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            
            if let jsonData = responseJson as? [String: Any]
            {
                
                if let myResults = jsonData["data"] as? [[String: Any]] {
                    
                    for value in myResults
                    {
                        if let gasStatus = value["gas_availability"] as? String {
                            self.dealers.gasAvailable = gasStatus
                            
                        }
                        
                        if let dealerName = value["name"] as? String{
                            self.dealers.name = dealerName
                        }
                        
                        if let dealerAddress = value["address"] as? String{
                            self.dealers.address = dealerAddress
                        }
                        
                        if let contactNo = value["mobileno"] as? String{
                            self.dealers.contact = contactNo
                        }
                        if let dealerId = value["uuid"] as? String{
                            self.dealers.uuid = dealerId
                        }
                        
                      self.allDealers.append(self.dealers)
                    }
                  
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                    }
                }
                
                if let myError = jsonData["status"] as? String{
                    if myError == "ERROR" {
                        if let message = jsonData["message"] as? String{
                            let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                                action in
                                self.navigationController?.popViewController(animated: true)
                            }
                            ))

                            self.present(alert, animated: true, completion: nil)
                        }}
                }
            }
        }
        task.resume()
    }
}
