//
//  AboutUsViewController.swift
//  ManojGas
//
//  Created by Mac on 4/22/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit
import WebKit

class AboutUsViewController: UIViewController {

    @IBOutlet weak var navBarContainerView: UIView!
    @IBOutlet weak var navBarContainerHC: NSLayoutConstraint!
    @IBOutlet weak var aboutUs: WKWebView!
    
    
    lazy var navigationBar: NavView = {
        let header = NavView()
        header.menuBtn.setImage(UIImage(named: "arrow-back"), for: .normal)
        header.titleLbl.isHidden = false
        header.titleLbl.text = "About Us"
        header.msgSettingBtn.isHidden = true
        header.contentView.backgroundColor = .clear
        header.menuBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
    }()
    
    @objc func popView() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupView() {
        navBarContainerView.addSubview(navigationBar)
        
        setupConstraints()
    }
    
    func setupConstraints(){
        var topConst: CGFloat = 20
        var navHeight: CGFloat = 44
        if hasTopNotch{
            topConst = 0
            navHeight *= 2
            navBarContainerHC.constant = navHeight
        }
        navigationBar.topAnchor.constraint(equalTo: navBarContainerView.topAnchor, constant: topConst).isActive = true
        navigationBar.leftAnchor.constraint(equalTo:  navBarContainerView.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo:  navBarContainerView.rightAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: navHeight).isActive = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        aboutUS()
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func aboutUS(){
        ViewControllerUtils().showActivityIndicator(uiView: self.view)
        let urlString = GlobalKey.baseUrl + "/aboutus"
        guard let url = URL(string: urlString)
            else {
                return
        }
        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data
                else { return }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                if let jsonData = json as? [String : Any]{
                    var resultString = ""
                    if let myResults = jsonData["data"] as? String{
                        resultString = myResults
                    }
                   
                    DispatchQueue.main.async {
                        let font = UIFont.init(name: Font.MyriadProRegular, size: Font.fontSize46)
                        self.aboutUs.loadHTMLString("<span style=\"font-family: \(font!.fontName); font-size: \(font!.pointSize) ; letter-spacing: 4px\">\(resultString)</span>", baseURL: nil)
                       ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                    }
                }
            }
            catch{
                print(error)
            }
            }
            .resume()
        
    }
}


