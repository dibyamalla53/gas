//
//  CustomView.swift
//  ManojGas
//
//  Created by f1soft on 11/12/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class RoundedButton: UIButton {
    @IBInspectable  var cornerRadius: CGFloat = 0 {
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
}

class RoundedView: UIView {
    @IBInspectable  var cornerRadius: CGFloat = 0 {
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable  var shadowColor: UIColor = .black {
        didSet{
            self.layer.shadowColor = shadowColor.cgColor
        }
    }
    
    @IBInspectable  var shadowOpacity: Float = 0 {
        didSet{
            self.layer.shadowOpacity = shadowOpacity
        }
    }
    
    @IBInspectable  var shadowOffset: CGSize = CGSize(width: 1, height: 1) {
        didSet{
            self.layer.shadowOffset = shadowOffset
        }
    }
    @IBInspectable  var shadowRadius: CGFloat = 0 {
        didSet{
            self.layer.shadowRadius = shadowRadius
        }
    }
}
