//
//  UserViewController.swift
//  ManojGas
//
//  Created by f1soft on 11/12/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import UIKit

class UserViewController: UIViewController {
    var result:String=""
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBOutlet weak var logoLbl: UILabel!{
        didSet{
            let text = NSLocalizedString("main-page-nav-title", comment: "MANOJ GAS")
            if Language.currentAppleLanguage() == LanguageOptions.english.rawValue {
                logoLbl.multipleColorText(text: text, range: NSRange(location: 6, length: 3), color: .black, font: UIFont(name: Font.MyriadProSemibold, size: Font.fontSize15))
            }
            logoLbl.text = text
            
        }
    }
    @IBOutlet weak var navImgView: UIImageView!{
        didSet{
            if hasTopNotch {
                navImgView.image = UIImage(named: "top-bg-x")
            }
        }
    }
    @IBOutlet weak var customerLoginBtn: RoundedButton!{
        didSet{
            let customerGrad = setGradient(topColor: UIColor(hexString: Color.CustomerButton.topGrad), bottomColor:UIColor(hexString: Color.CustomerButton.bottomGrad), frame:CGRect(x: 0, y: 0, width: 100, height: 100), cornerRadius: 6)
            customerLoginBtn.layer.insertSublayer(customerGrad, below: customerLoginBtn.titleLabel?.layer)
            let imgView = UIImageView(frame: CGRect(x: customerLoginBtn.bounds.size.width/4, y: 8, width: customerLoginBtn.bounds.size.width/2, height: customerLoginBtn.bounds.size.height/2))
            imgView.contentMode = UIView.ContentMode.scaleAspectFit
            imgView.image = UIImage(named: "customer")
            customerLoginBtn.addSubview(imgView)
        }
    }
    @IBOutlet weak var dealerLoginBtn: RoundedButton!{
        didSet{
            let customerGrad = setGradient(topColor: UIColor(hexString: Color.DealerButton.topGrad), bottomColor:UIColor(hexString: Color.DealerButton.bottomGrad), frame:CGRect(x: 0, y: 0, width: 100, height: 100), cornerRadius: 6)
            dealerLoginBtn.layer.insertSublayer(customerGrad, below: dealerLoginBtn.titleLabel?.layer)
            let imgView = UIImageView(frame: CGRect(x: dealerLoginBtn.bounds.size.width/4, y: 8, width: dealerLoginBtn.bounds.size.width/2, height: dealerLoginBtn.bounds.size.height/2))
            imgView.contentMode = UIView.ContentMode.scaleAspectFit
            imgView.image = UIImage(named: "dealer")
            dealerLoginBtn.addSubview(imgView)
        }
    }
    
    
    var notification: [AnyHashable: Any]? {
       
        didSet {
            Logger.log(message: "for notification: \(String(describing: notification))", event:  .i)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
   
    @IBAction func customerAction(_ sender: UIButton) {
        Helper().setValue(value: UserType.customer.rawValue, key: Key.user)
    }
    @IBAction func dealerAction(_ sender: UIButton) {
        Helper().setValue(value: UserType.dealer.rawValue, key: Key.user)
    }

    
    
    
    
}
