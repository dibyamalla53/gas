//
//  LoginViewController.swift
//  ManojGas
//
//  Created by f1soft on 11/14/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController{
   
    let userType = Helper().getValueForKey(key: Key.user)
 
    lazy var navigationBar: NavView = {
        let header = NavView()
        header.menuBtn.setImage(UIImage(named: "arrow-back"), for: .normal)
        header.msgSettingBtn.isHidden = true
        header.menuBtn.addTarget(self, action: #selector(popAction), for: .touchUpInside)
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
    }()
    @IBOutlet weak var logoLbl: UILabel!{
        didSet{
            logoLbl.multipleColorText(text: "MANOJ GAS", range: NSRange(location: 6, length: 3), color: .black, font: UIFont(name: Font.MyriadProSemibold, size: Font.fontSize15))
        }
    }
    @IBOutlet weak var navBarContainerView: UIView!
    @IBOutlet weak var navBarContainerHC: NSLayoutConstraint!
    @IBOutlet weak var usernameTxtF: UITextFieldX!{
        didSet{
            usernameTxtF.rightView?.isHidden = true
        }
    }
    @IBOutlet weak var passwordTxtF: UITextFieldX!{
        didSet{
            passwordTxtF.rightView?.isHidden = true
        }
    }
    @IBOutlet weak var rememberMeBtn: UIButton!{
        didSet{
            rememberMeBtn.setImage(UIImage(named : "remember-me-unselected"), for: .normal)
            rememberMeBtn.setImage(UIImage(named : "remember-me-selected"), for: .selected)
        }
    }
    @IBOutlet weak var loginBtn: RoundedButton!{
        didSet{
            let customerGrad = setGradient(topColor: UIColor(hexString: Color.CustomerButton.topGrad), bottomColor:UIColor(hexString: Color.CustomerButton.bottomGrad), frame:CGRect(x: 0, y: 0, width: screenSize.width-16, height: 40), cornerRadius: 20)
            loginBtn.layer.insertSublayer(customerGrad, below: loginBtn.titleLabel?.layer)
        }
    }
    @IBOutlet weak var registerBtn: UIButton!{
        didSet{
            let text = "Don't have an account? REGISTER"
            let mutableString = NSMutableAttributedString(string: text)
            
            mutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: Font.MyriadProRegular, size: Font.fontSize15)!, range:NSRange(location: 22, length: 9))
            mutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:UIColor(hexString: Color.CustomerButton.topGrad), range:NSRange(location: 22, length: 9))
            mutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:UIColor(hexString: Color.text_dark_gray), range:NSRange(location: 0, length: 22))
            registerBtn.setAttributedTitle(mutableString, for: .normal)
        }
    }
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    var activeField: UITextField?
    var user: String? = Helper().getValueForKey(key: Key.user)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
        view.backgroundColor = UIColor(hexString: Color.backgroundColor)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // Add touch gesture for contentView
        self.scrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(returnTextView)))
        
        if UserDefaults.standard.bool(forKey: "rememberMeBtn"){
            usernameTxtF.text=UserDefaults.standard.string(forKey: "username")
            passwordTxtF.text=UserDefaults.standard.string(forKey: "password")
            rememberMeBtn.isSelected=true
        }
        
      
        
    }
    
    @objc func returnTextView() {
        guard activeField != nil else {
            return
        }
        
        activeField?.resignFirstResponder()
        activeField = nil
    }
    
    private func setupView() {
        navBarContainerView.addSubview(navigationBar)
        if let user = self.user {
            switch user {
            case UserType.customer.rawValue:
                navigationBar.titleLbl.text = "Customer login"
            case UserType.dealer.rawValue:
                navigationBar.titleLbl.text = "Dealer login"
            default:
                Logger.log(message: "user not logged in as", event: .i)
            }
        }
        setupContstraints()
    }
    
    private func setupContstraints() {
        var topConst: CGFloat = 20
        var navHeight: CGFloat = 44
        if hasTopNotch {
            topConst = 0
            navHeight *= 2
            navBarContainerHC.constant = navHeight
        }
        navigationBar.topAnchor.constraint(equalTo: navBarContainerView.topAnchor, constant:topConst).isActive = true
        navigationBar.leftAnchor.constraint(equalTo:  navBarContainerView.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo:  navBarContainerView.rightAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: navHeight).isActive = true
    }
    
    @objc func popAction() {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func rememberAction(_ sender: UIButton) {
        if rememberMeBtn.isSelected == true {
            
            rememberMeBtn.isSelected = false
            
        }else {
            rememberMeBtn.isSelected = true
            
            
            
            
        }
    }
    @IBAction func loginAction(_ sender: UIButton) {
        returnTextView()
        
        if self.usernameTxtF.text!.isEmpty || self.passwordTxtF.text!.isEmpty {
            let alertController = UIAlertController(title: "Error", message: "Please Enter the Username and Password", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
            //          usernameTxtF.rightView?.isHidden = false
            
        }
        else {
            ViewControllerUtils().showActivityIndicator(uiView: self.view)
          
            let url = URL(string: GlobalKey.baseUrl + "/login")!
            let user = Helper().getValueForKey(key: Key.user)
            let regid = Helper().getValueForKey(key: GlobalKey.regid)
            

            let parameterDictionary = ["username" : usernameTxtF.text!, "password": passwordTxtF.text!, "logintype": user, "regid": regid, "ostype": "ios", "osversion": "", "deviceid": "", "appversion": ""  ]

            Logger.log(message: "UserType:\(String(describing: user))", event: .i)
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Application/json", forHTTPHeaderField: "Accept")
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: []) else {
                return
            }
            request.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
                
                if let data = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                        if let data = json as? [String:Any]{
                            Logger.log(message: "data:: \(String(describing: data))", event: .i)
                            Helper().setValue(value: self.userType!, key: GlobalKey.userType)
                            if let apitokenData = data["data"] as? [String:Any]{
                                DispatchQueue.main.async {
                                    
                                    if self.rememberMeBtn.isSelected
                                    {
                                        UserDefaults.standard.set(true,forKey: "rememberMeBtn")
                                        UserDefaults.standard.set(self.usernameTxtF.text!, forKey: "username")
                                        UserDefaults.standard.set(self.passwordTxtF.text!, forKey: "password")
                                        
                                    }
                                    
                                    if let apitoken = apitokenData["api_token"] as? String{
                                        Helper().setValue(value: apitoken, key: GlobalKey.apiToken)
                                        
                                        guard let apitoken = UserDefaults.standard.string(forKey: "api_token") else {return}
                                        print("savetoken  is: \(apitoken)")
                                    }
                                    
                                    if let name=apitokenData["name"] as? String{
                                        Helper().setValue(value:name,key: GlobalKey.customername)
                                        print("name  is: \(name)")
                                        
                                    }
                                    
                                    if let email=apitokenData["email"] as? String{
                                        Helper().setValue(value:email,key: GlobalKey.customeremail)
                                        print("email  is: \(email)")
                                    }
                                    
                                    if let address=apitokenData["address"] as? String{
                                        Helper().setValue(value:address,key: GlobalKey.customeraddress)
                                        print("address  is: \(address)")
                                    }
                                    
                                    if let phoneno=apitokenData["mobileno"] as? String{
                                        Helper().setValue(value:phoneno,key: GlobalKey.phoneno)
                                        print("phoneno  is: \(phoneno)")
                                    }
                                    
                                    
                                    if let message = data["message"] as? String{
                                        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                                            action in
                                            appDelegate.setDrawerRootViewController(usingAnimation: .present,andNotification: nil)
                                         
                                        }))
                                        
                                        self.present(alert, animated: true, completion: nil)
                                   
                                       ViewControllerUtils().hideActivityIndicator(uiView: self.view)

                                    }
                                }
                                
                            }
                            DispatchQueue.main.async {
                                if let message = data["message"] as? String{
                                    let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil
                                    ))
                                    

                                    self.present(alert, animated: true, completion: nil)
                                    ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                                }
                               
                            }
                           
                        }
                        
                        print(json)
                    }catch {
                        print(error)
                    }
                    
                }
               
                }.resume()
            
        }
        
    }
    @IBAction func registerAction(_ sender: UIButton) {
        returnTextView()
        let vc=self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController")as! RegisterViewController
        //            let vc = FeedbackViewController()
        self.navigationController?.pushViewController(vc, animated: true)
        
        Logger.log(message: "register action", event: .i)
    }
    
}


extension LoginViewController {
    @objc func keyboardWillShow(notification: NSNotification) {
        
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            // so increase contentView's height by keyboard height
            UIView.animate(withDuration: 0.3, animations: {
                //self.constraintContentHeight.constant += self.keyboardHeight
                let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
                self.scrollView.contentInset = contentInsets
                self.scrollView.scrollIndicatorInsets = contentInsets
            })
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.3) {
            self.scrollView.contentInset = .zero
            self.scrollView.scrollIndicatorInsets = .zero
        }
    }
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeField = textField
    }
    //Note. Be careful when you use a secured text field.
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            //            Logger.log(message: "updatedText count: \(updatedText.count)", event: .i)
            if textField == self.usernameTxtF {
                if updatedText.count > 0 {
                    usernameTxtF.rightView?.isHidden = true
                }
                
            }
            else if textField == self.passwordTxtF {
                if updatedText.count > 0 {
                    passwordTxtF.rightView?.isHidden = true
                }
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        activeField = nil
        return true
    }
    
}
