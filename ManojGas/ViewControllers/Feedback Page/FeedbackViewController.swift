//
//  FeedbackViewController.swift
//  ManojGas
//
//  Created by f1soft on 11/15/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController ,UITextViewDelegate{
    
    lazy var navigationBar: NavView = {
        let header = NavView()
        header.menuBtn.setImage(UIImage(named: "arrow-back"), for: .normal)
        header.titleLbl.isHidden = true
        header.contentView.backgroundColor = .clear
        header.msgSettingBtn.isHidden = true
        header.menuBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
        
    }()
    @IBOutlet weak var navBarContainerView: UIView!
    @IBOutlet weak var navBarContainerHC: NSLayoutConstraint!
    @IBOutlet weak var taskNotes: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    var activeField: UITextField?
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var subjectTxt: UITextField!{
        didSet{
            subjectTxt.rightView?.isHidden = true
        }
    }
    @IBOutlet weak var imageView: UIImageView!
        {
        didSet{
            
            imageView.isHidden = true
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
        self.view.backgroundColor = UIColor(hexString: Color.backgroundColor)
        let borderColor : UIColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1.0)
        taskNotes.layer.borderWidth = 0.5
        taskNotes.tintColor = UIColor.darkGray
        taskNotes.layer.borderColor = borderColor.cgColor
        taskNotes.layer.cornerRadius = 5.0
        setupViews()
        
        self.scrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(returnTextView)))
        
        taskNotes.text="Message"
        taskNotes.textColor=UIColor.lightGray
        
        taskNotes.font=UIFont( name:"MyriadProSemibold", size: 14.0)
        taskNotes.returnKeyType = .done
        taskNotes.delegate=self
     
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text=="Message"{
            textView.text=""
            textView.textColor=UIColor.black
            textView.font=UIFont( name:"MyriadProSemibold", size: 20.0)
            imageView.isHidden=true
            
        }
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text==""{
            textView.text="Message"
            textView.textColor=UIColor.lightGray
            textView.font=UIFont( name:"MyriadProSemibold", size: 20.0)
            
        }
    }
    
    
    
    @objc func returnTextView() {
        view.endEditing(true)
        
        
        // activeField?.resignFirstResponder()
        //activeField = nil
    }
    
    
    @IBAction func sendBtn(_ sender: UIButton) {
        
        //indicator or progress dailog bar
        
        
        
        returnTextView()
        if self.subjectTxt.text == "" {
            subjectTxt.rightView?.isHidden=false
             ViewControllerUtils().hideActivityIndicator(uiView: self.view)
            
            
            
        }else if self.taskNotes.text == "Message"{
            
           ViewControllerUtils().hideActivityIndicator(uiView: self.view)
            imageView.isHidden=false
            
        }else if self.taskNotes.text==""{
            imageView.isHidden=false
            ViewControllerUtils().hideActivityIndicator(uiView: self.view)
        }
            
            
        else {
            ViewControllerUtils().showActivityIndicator(uiView: self.view)
            
            let url = URL(string: GlobalKey.baseUrl + "/feedback/post")!
            let apitoken = Helper().getValueForKey(key: GlobalKey.apiToken)!
            let name = Helper().getValueForKey(key: GlobalKey.customername)!
            let email = Helper().getValueForKey(key: GlobalKey.customeremail)!
            let messagetext=taskNotes.text!
            let titletxt=subjectTxt.text!
            
            let parameterDictionary = ["api_token": apitoken, "fullname":name,
                                       "email": email,
                                       "subject": titletxt,
                                       "message":messagetext] as [String : Any]
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: [])
                else {
                return
            }
            request.httpBody = httpBody
            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
                if let response = response {
                    
                    print("this is \(response)")
                }
                if let data = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                        if let data = json as? [String:Any]{
                            Logger.log(message: "data:: \(data)", event: .i)
                            
                            DispatchQueue.main.async {
                                if let message = data["message"] as? String
                                {
                                    let alert = UIAlertController(title: "", message:message, preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                                        action in
                                        self.navigationController?.popToRootViewController(animated: true)
                                    }))
                                    self.present(alert, animated: true)
                                    print(json)
                                    ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                                }
                                
                            }
                            
                            
                        }
                        
                    }
                    catch {
                        print(error)
                    }
                }
                
                }.resume()
        }
        
        
        
        
    }
    
    
    
    @IBOutlet weak var sendBtn: RoundedButton!{
        didSet{
            let customerGrad = setGradient(topColor: UIColor(hexString: Color.CustomerButton.topGrad), bottomColor:UIColor(hexString: Color.CustomerButton.bottomGrad), frame:CGRect(x: 0, y: 0, width: screenSize.width-121, height: 40), cornerRadius: 20)
            sendBtn.layer.insertSublayer(customerGrad, below: sendBtn.titleLabel?.layer)
        }
    }
    
    
    @objc func popView() {
        self.navigationController?.popViewController(animated: true)
        
    }
    
}

extension FeedbackViewController {
    private func setupViews() {
        
        navBarContainerView.addSubview(navigationBar)
        setupConstraints()
    }
    
    private func setupConstraints() {
        
        
        var topConst: CGFloat = 20
        var navHeight: CGFloat = 44
        if hasTopNotch {
            topConst = 0
            navHeight *= 2
            navBarContainerHC.constant = navHeight
        }
        navigationBar.topAnchor.constraint(equalTo: navBarContainerView.topAnchor, constant:topConst).isActive = true
        navigationBar.leftAnchor.constraint(equalTo:  navBarContainerView.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo:  navBarContainerView.rightAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: navHeight).isActive = true
        
    }
    
}




extension FeedbackViewController: UITextFieldDelegate {
    
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeField = textField
    }
    
    //Note. Be careful when you use a secured text field.
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            //            Logger.log(message: "updatedText count: \(updatedText.count)", event: .i)
            if textField == self.subjectTxt {
                if updatedText.count > 0 {
                    subjectTxt.rightView?.isHidden = true
                }
                
            }
            
        }
        return true
    }
    
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        activeField=nil
        return true
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
extension FeedbackViewController {
    
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            // so increase contentView's height by keyboard height
            UIView.animate(withDuration: 0.3, animations: {
                //self.constraintContentHeight.constant += self.keyboardHeight
                let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
                self.scrollView.contentInset = contentInsets
                self.scrollView.scrollIndicatorInsets = contentInsets
            })
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.3) {
            
            
            self.scrollView.contentInset = .zero
            self.scrollView.scrollIndicatorInsets = .zero
        }
    }
}
