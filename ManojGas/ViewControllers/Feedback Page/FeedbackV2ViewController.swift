//
//  FeedbackV2ViewController.swift
//  ManojGas
//
//  Created by Mac on 11/16/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import UIKit
import Foundation

class FeedbackV2ViewController: UIViewController {

    @IBOutlet weak var sendButton: UIButton!{
        didSet{
            sendButton.layer.cornerRadius = sendButton.frame.height/2
            sendButton.clipsToBounds = true
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
