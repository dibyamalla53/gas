//
//  SelectLocationFromMapViewController.swift
//  ManojGas
//
//  Created by Mac on 11/27/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

protocol SelectLocationFromMapViewControllerDelegate {
    func didLocationSelected(latitude: Double,longitude: Double, location:String)
}

class SelectLocationFromMapViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    var delegate:SelectLocationFromMapViewControllerDelegate?
    let locationManager=CLLocationManager()

    var latitude:Double?
    var longitude:Double?
    var location:String?
    var camera = GMSCameraPosition()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.mapView.delegate = self
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.showCurrentLocationMap()
        self.locationManager.stopUpdatingLocation()
    }
    
    func showCurrentLocationMap(){
        let camera=GMSCameraPosition.camera(withLatitude: (self.locationManager.location?.coordinate.latitude)!, longitude:(self.locationManager.location?.coordinate.latitude)!, zoom: 14.0)
        
        let mapView = GMSMapView.map(withFrame: self.view.bounds, camera: camera)
        
        mapView.settings.myLocationButton=true
        mapView.isMyLocationEnabled=true
        let marker=GMSMarker()
        marker.position=camera.target
        
        marker.snippet="current location"
        marker.map=mapView
        
        self.view.addSubview(mapView)
    }
    
    
    @IBAction func chooseLocationBtnPressed(_ sender: UIButton) {
        
        if let latitude = self.latitude, let longitude = self.longitude, let location = self.location {
            self.delegate?.didLocationSelected(latitude: latitude, longitude: longitude, location: location)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        //get center position
        //get location
        
    }
}

