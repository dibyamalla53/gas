//
//  AgreeViewController.swift
//  ManojGas
//
//  Created by Mac on 11/27/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import UIKit
import WebKit

class Web: WKWebView {
    override init(frame: CGRect, configuration: WKWebViewConfiguration) {
        super.init(frame: frame, configuration: configuration)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}


class AgreeViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        termsAndCondition()
    }
    var conditions = ""
 
    @IBAction func okBtn(_ sender:UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var terms: WKWebView!
    
    func termsAndCondition(){
        let urlString = GlobalKey.baseUrl + "/termsandconditions"
        guard let url = URL(string: urlString)
            else {
                return
        }
        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data
                else { return }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                if let jsonData = json as? [String: Any]{
                    if let myResult = jsonData["data"] as? String{
                        self.conditions = myResult
                    }
                    DispatchQueue.main.async {
            
                        let font = UIFont.init(name: Font.MyriadProRegular, size: Font.fontSize46)
                        self.terms.loadHTMLString("<span style=\"font-family: \(font!.fontName); font-size: \(font!.pointSize)\">\(self.conditions)</span>", baseURL: nil)
                   
                    }
                }
            }
            catch{
                print(error)
            }
            }
            .resume()
    }
    
    
}


