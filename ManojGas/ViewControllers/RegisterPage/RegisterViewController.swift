//
//  RegisterViewController.swift
//  ManojGas
//
//  Created by Mac on 11/19/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var trackLocation: UIButton!{
        didSet{
            trackLocation.setTitle("find", for: .normal)
        }
    }
    @IBAction func trackLocation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "mapScreenViewController") as! mapScreenViewController
        vc.location = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBOutlet weak var singUplabel: UILabel!{
        didSet{
            let u = Helper().getValueForKey(key:GlobalKey.userType)
            Logger.log(message: "uer\(String(describing: u))", event: .i)
//             Logger.log(message: "data:: \(String(describing: data))", event: .i)
            var text:String=""
            if(u=="dealer")
            {
                  text = NSLocalizedString("DEALER SIGN UP", comment: "DEALER SIGNUP")
            }
            else{
                  text = NSLocalizedString("CUSTOMER SIGN UP", comment: "CUSTOMER SIGNUP")
            }
//            if Language.currentAppleLanguage() == LanguageOptions.english.rawValue {
//                singUplabel.multipleColorText(text: text, range: NSRange(location: 8, length: 8), color: .black, font: UIFont(name: Font.MyriadProSemibold, size: Font.fontSize15))
//            }
            singUplabel.text = text
            
        }
    }
    @IBOutlet weak var scrollView: UIScrollView!
    lazy var navigationBar: NavView = {
        let header = NavView()
        header.menuBtn.setImage(UIImage(named: "arrow-back"), for: .normal)
        header.titleLbl.isHidden = true
        header.contentView.backgroundColor = .clear
        header.msgSettingBtn.isHidden = true
        header.menuBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
        
    }()
    @IBOutlet weak var navBarContainerView: UIView!
    @IBOutlet weak var navBarContainerHC: NSLayoutConstraint!
    override func viewDidLoad() {
    super.viewDidLoad()
    
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    
    setupViews()
    Logger.log(message: "max y button: \(self.signUpBtn.frame.maxY)", event: .i)
    
    self.scrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(returnTextView)))
}
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Logger.log(message: "is agreee buttton selected: \(checkboxBtn.isSelected)", event: .i)
    }
    
    @objc func returnTextView() {
        guard activeField != nil else {
            return
        }
        
        activeField?.resignFirstResponder()
        activeField = nil
    }
   
    @IBOutlet weak var signUpBtn: RoundedButton!{
        didSet{
            let customerGrad = setGradient(topColor: UIColor(hexString: Color.CustomerButton.topGrad), bottomColor:UIColor(hexString: Color.CustomerButton.bottomGrad), frame:CGRect(x: 0, y: 0, width: screenSize.width-16, height: 40), cornerRadius: 20)
            signUpBtn.layer.insertSublayer(customerGrad, below: signUpBtn.titleLabel?.layer)
        }
        
    }
    @IBAction func signUp(_ sender: UIButton) {
        
        if self.nameField.text == "" {
            nameField.rightView?.isHidden=false
            
        }
        else if self.usernameField.text == "" {
            usernameField.rightView?.isHidden = false
            
        }
        else if self.passwordField.text == "" {
            passwordField.rightView?.isHidden = false
        }
        else if self.mobilenoField.text == "" {
            mobilenoField.rightView?.isHidden = false
        }
        else if self.phonenoField.text == "" {
            phonenoField.rightView?.isHidden = false
        }
        else if self.emailField.text == "" {
            emailField.rightView?.isHidden = false
        }
            
        else if (!self.checkboxBtn.isSelected) {
            let check = UIAlertController(title: "You have not agree with terms yet ",
                                          message: "",
                                          preferredStyle: .alert)
            check.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                
            }))
            present(check,animated: true,completion: nil)
        }
            
        else {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
            let url = URL(string: GlobalKey.baseUrl + "/register")!
            
            let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
            let udid = UIDevice.current.identifierForVendor!.uuidString
            let regid = UUID().uuidString
            Helper().setValue(value: regid, key: GlobalKey.regid)
            
            let lat: Double = Helper().getDoubleValue(forKey: GlobalKey.latitude) ?? 0
            let lon: Double = Helper().getDoubleValue(forKey: GlobalKey.longitude) ?? 0
            let user = Helper().getValueForKey(key: Key.user)
            let parameterDictionary: [String: Any] = ["name": nameField.text!,
                                       "username": usernameField.text!,
                                       "password": passwordField.text!,
                                       "usertype": user as Any ,
                                       "mobileno":mobilenoField.text!,
                                       "address": trackLocation.titleLabel?.text! as Any,
                                       "latitude": lat,
                                       "longitude": lon,
                                       "email": emailField.text!,
                                       "regid": regid,
                                       "ostype": "ios",
                                       "osversion": getOSInfo(),
                                       "deviceid": udid,
                                       "appversion": appVersion ]
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary.self, options: []) else {
                return
            }
            request.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
                if let response = response {
                    
                    print("this is \(response)")
                }
                if let data = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                        if let data = json as? [String:Any]{
                            Logger.log(message: "data:: \(data)", event: .i)
                        }
                        DispatchQueue.main.async {
                            UserDefaults.standard.set(self.usernameField.text!, forKey: "username")
                            UserDefaults.standard.set(self.passwordField.text!, forKey: "password")
                            UserDefaults.standard.set(self.emailField.text!, forKey: "email")
                            UserDefaults.standard.synchronize()
                            guard let saveUservalue = UserDefaults.standard.string(forKey: "username") else {return}
                            Logger.log(message:"username is: \(saveUservalue)", event: .i)
                            guard let savePassword = UserDefaults.standard.string(forKey: "password") else {return}
                            Logger.log(message:"password is: \(savePassword)", event: .i)
                            guard let saveEmail = UserDefaults.standard.string(forKey: "email") else {return}
                            Logger.log(message:"email is: \(saveEmail)", event: .i)
                            print(json)
                        }
                    }catch {
                        print(error)
                    }
                }
                }.resume()
        }
        
        
        
        
        
        
        
    }
    
    func getOSInfo()->String {
        let os = ProcessInfo().operatingSystemVersion
        return String(os.majorVersion) + "." + String(os.minorVersion) + "." + String(os.patchVersion)
    }
    
    @IBOutlet weak var nameField: UITextFieldX!{
        didSet{
            nameField.rightView?.isHidden = true
        }
    }
    @IBOutlet weak var usernameField: UITextFieldX!{
        didSet{
            
            usernameField.rightView?.isHidden=true
        }
    }
    @IBOutlet weak var passwordField: UITextFieldX!{
        didSet{
            
            passwordField.rightView?.isHidden=true
        }
    }
    @IBOutlet weak var mobilenoField: UITextFieldX!{
        didSet{
            mobilenoField.rightView?.isHidden=true
        }
    }
    @IBOutlet weak var phonenoField: UITextFieldX!{
        didSet{
            phonenoField.rightView?.isHidden=true
        }
    }
    @IBOutlet weak var emailField: UITextFieldX!{
        
        didSet{
            emailField.rightView?.isHidden=true
        }
    }
    @IBOutlet weak var checkboxBtn: UIButton!{
        didSet{
            checkboxBtn.setImage(UIImage(named : "remember-me-unselected"), for: .normal)
            checkboxBtn.setImage(UIImage(named : "remember-me-selected"), for: .selected)
        }}
    @IBAction func rememberAction(_ sender: UIButton) {
        
        
        //   let popup = self.storyboard?.instantiateViewController(withIdentifier: "AgreeViewController") as! AgreeViewController
        //       self.addChild(popup)
        //  popup.view.frame=self.view.frame
        //    self.view.addSubview(popup.view)
        
        
        if !checkboxBtn.isSelected{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AgreeViewController") as! AgreeViewController
            
            self.present(vc, animated: true, completion: nil)
            checkboxBtn.isSelected = true
        }else {
            checkboxBtn.isSelected = false
        }
    }
    @objc func popView() {
        self.navigationController?.popViewController(animated: true)
        
    }
    
}

extension RegisterViewController: LocationDelegate{
    
    func updateAddress(location: String) {
        trackLocation.setTitle(location, for: .normal)
    }
}

extension RegisterViewController {
    private func setupViews() {
        
        navBarContainerView.addSubview(navigationBar)
        setupConstraints()
    }
    
    private func setupConstraints() {
    
        var topConst: CGFloat = 20
        var navHeight: CGFloat = 44
        if hasTopNotch {
            topConst = 0
            navHeight *= 2
            navBarContainerHC.constant = navHeight
        }
        navigationBar.topAnchor.constraint(equalTo: navBarContainerView.topAnchor, constant:topConst).isActive = true
        navigationBar.leftAnchor.constraint(equalTo:  navBarContainerView.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo:  navBarContainerView.rightAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: navHeight).isActive = true
        
    }
}


func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    
    return true
}
extension RegisterViewController {
   
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            // so increase contentView's height by keyboard height
            UIView.animate(withDuration: 0.3, animations: {
                //self.constraintContentHeight.constant += self.keyboardHeight
                let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
                self.scrollView.contentInset = contentInsets
                self.scrollView.scrollIndicatorInsets = contentInsets
                
            })
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.3) {
            
            
            self.scrollView.contentInset = .zero
            self.scrollView.scrollIndicatorInsets = .zero
        }
    }
}
var activeField: UITextField?
extension RegisterViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeField = textField
    }
    
    //Note. Be careful when you use a secured text field.
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            //            Logger.log(message: "updatedText count: \(updatedText.count)", event: .i)
            if textField == self.nameField {
                if updatedText.count > 0 {
                    nameField.rightView?.isHidden = true
                }
            }
            else if textField == self.usernameField {
                if updatedText.count > 0 {
                    usernameField.rightView?.isHidden = true
                }
            }
            else if textField == self.passwordField {
                if updatedText.count > 0 {
                    passwordField.rightView?.isHidden = true
                }
            }
            else if textField == self.mobilenoField {
                if updatedText.count > 0 {
                    mobilenoField.rightView?.isHidden = true
                }
            }
            else if textField == self.phonenoField {
                if updatedText.count > 0 {
                    phonenoField.rightView?.isHidden = true
                }
            }
                
            else if textField == self.emailField {
                if updatedText.count > 0 {
                    emailField.rightView?.isHidden = true
                }
            }
        }
        
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        activeField = nil
        return true
    }
}

