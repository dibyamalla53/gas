//
//  mapScreenViewController.swift
//  ManojGas
//
//  Created by Mac on 4/8/19.
//  Copyright © 2019 shirantech. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

protocol LocationDelegate {
    func updateAddress(location: String)
}

class mapScreenViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    
    @IBOutlet weak var navBarContainerView: UIView!
    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var navBarContainerHC: NSLayoutConstraint!
    var mapView: GMSMapView!
    var camera: GMSCameraPosition!
    var location: LocationDelegate!
    lazy var navigationBar: NavView = {
        let header = NavView()
        header.menuBtn.setImage(UIImage(named: "arrow-back"), for: .normal)
        header.titleLbl.isHidden = false
        header.contentView.backgroundColor = .clear
        header.msgSettingBtn.isHidden = true
        header.menuBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
    }()
    var lat: Double!
    var lon: Double!
    @objc func popView() {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func setupView() {
        navBarContainerView.addSubview(navigationBar)
        navigationBar.titleLbl.text = "My Map"
        Logger.log(message: "map not loaded", event: .i)
        setupConstraints()
    }
    
    func setupConstraints(){
        var topConst: CGFloat = 20
        var navHeight: CGFloat = 44
        if hasTopNotch{
            topConst = 0
            navHeight *= 2
            navBarContainerHC.constant = navHeight
        }
        navigationBar.topAnchor.constraint(equalTo: navBarContainerView.topAnchor, constant: topConst).isActive = true
        navigationBar.leftAnchor.constraint(equalTo:  navBarContainerView.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo:  navBarContainerView.rightAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: navHeight).isActive = true
        
    }
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        setupView()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.showCurrentLocationOnMap()
        self.locationManager.stopUpdatingLocation()
        if let location = locations.first
        {
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        print("Tapped at coordinate: " + String(coordinate.latitude) + " "
            + String(coordinate.longitude))
        self.lat = coordinate.latitude
       // Helper().setDoubleValue(value: self.lat, key: "latitude")
        self.lon = coordinate.longitude
     //   Helper().setDoubleValue(value: self.lon, key: "longitude")
        
        geocode(latitude: self.lat, longitude: self.lon, completion:{ (placemark, error) in
            if error != nil {
                print("Error occured")
            }
            else if let placemark = placemark?.first {
                DispatchQueue.main.async {
                    guard let address =  placemark.name
                        else {
                            return
                    }
                    print("Street: \(String(describing: placemark))")
                    self.location.updateAddress(location: address)
                    self.navigationController?.popViewController(animated: true)
                    
                }
            }
        })
        
        
    }
    
    func geocode(latitude: Double, longitude: Double, completion: @escaping (_ placemark: [CLPlacemark]?, _ error: Error?) -> Void)  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude), completionHandler: completion)
    }
    func showCurrentLocationOnMap(){
        self.camera = GMSCameraPosition.camera(withLatitude: (self.locationManager.location?.coordinate.latitude)!, longitude: (self.locationManager.location?.coordinate.longitude)!, zoom: 14)
        
        self.mapView = GMSMapView.map(withFrame: CGRect(x:0, y:0, width: self.myView.frame.size.width, height: self.myView.frame.size.height), camera: camera)
        
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        let marker = GMSMarker()
        marker.position = camera.target
        marker.snippet = "Current Location"
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.map = mapView
        self.myView.addSubview(mapView)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "map" {
            if let vc = segue.destination as? mapScreenViewController {
                Logger.log(message: "vc: \(vc)", event: .i)
            }
        }
    }
    
    
}

