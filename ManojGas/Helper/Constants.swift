//
//  Constants.swift
//  ManojGas
//
//  Created by f1soft on 11/12/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import Foundation
import UIKit

struct Font {
    
        static let MyriadProCond = "MyriadPro-Cond"
        static let MyriadProSemibold = "MyriadPro-Semibold"
        static let MyriadProBold = "MyriadPro-Bold"
        static let MyriadProLight = "MyriadPro-Light"
        static let MyriadProRegular = "MyriadPro-Regular"
    
        static let fontSize16: CGFloat = 16.0
        static let fontSize15: CGFloat = 15.0
        static let fontSize14: CGFloat = 14.0
        static let fontSize13: CGFloat = 13.0
        static let fontSize12: CGFloat = 12.0
        static let fontSize11: CGFloat = 11.0
        static let fontSize10: CGFloat = 10.0
        
        
        static let fontSize24: CGFloat = 24.0
        static let fontSize28: CGFloat = 28.0
        static let fontSize32: CGFloat = 32.0
        static let fontSize46: CGFloat = 46.0
}

struct Color {
    struct CustomerButton {
        static let topGrad = "#31A345"
        static let bottomGrad = "#227B71"
    }
    
    struct DealerButton {
        static let topGrad = "#FE3419"
        static let bottomGrad = "#921C0D"
    }
    
    static let backgroundColor = "#FAFAFA"
    static let text_dark_gray = "#4F4F4F"
}

struct Key {
    static let user = "user"
    static let language = "lang"
}

enum LanguageOptions: String {
    case nepali="ne"
    case english="en"
}


