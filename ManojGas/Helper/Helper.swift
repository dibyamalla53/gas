//
//  Helper.swift
//  ManojGas
//
//  Created by f1soft on 11/12/18.
//  Copyright © 2018 shirantech. All rights reserved.
//

import Foundation
import UIKit
var screenSize:CGSize {
    return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
}

var appDelegate: AppDelegate {
    return UIApplication.shared.delegate as! AppDelegate
}

var storyBoard: UIStoryboard {
    return UIStoryboard(name: "Main", bundle: nil)
}


var navigationController: UINavigationController{
    return UINavigationController()
}

var hasTopNotch: Bool {
    if #available(iOS 11.0, tvOS 11.0, *) {
        // with notch: 44.0 on iPhone X, XS, XS Max, XR.
        // without notch: 20.0 on iPhone 8 on iOS 12+.
        return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
    }
    return false
}

func multipleColorText(text: String, range: NSRange, color: UIColor, font: UIFont? = nil) -> NSMutableAttributedString {
    
    let myMutableString = NSMutableAttributedString(string: text)
    if let font = font {
        myMutableString.addAttribute(NSAttributedString.Key.font, value: font, range:range)
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:color, range:range)
    }else {
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:color, range:range)
    }
    
    // self.attributedText = myMutableString
    return myMutableString
}

func setGradient(topColor: UIColor, bottomColor: UIColor, frame: CGRect, cornerRadius: CGFloat? = nil) -> CAGradientLayer
{
    let gradientLayer = CAGradientLayer()
    gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
    gradientLayer.startPoint = .zero
    gradientLayer.endPoint = CGPoint(x: 0, y: 1)
    gradientLayer.frame = frame
    if let cRadius = cornerRadius {
        gradientLayer.cornerRadius  = cRadius
    }
    
    //    if let topLayer = view.layer.sublayers?.first, topLayer is CAGradientLayer
    //    {
    //        topLayer.removeFromSuperlayer()
    //    }
    return gradientLayer
}


enum UserType: String {
    case customer, dealer
}

struct Helper {
    
    
    func setValue(value: String, key: String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
        defaults.synchronize()
    }
    func setBoolValue(value: Bool, key: String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
        defaults.synchronize()
    }
    
    func setDoubleValue(value: Double, key: String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
        defaults.synchronize()
    }
    
    func setObject(value: [String: String], key: String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
        defaults.synchronize()
    }
    
    func getObjectForKey(key: String) -> Any? {
        let defaults = UserDefaults.standard
        return defaults.value(forKey: key)
    }
    
    func getValueForKey(key: String) -> String? {
        let defaults = UserDefaults.standard
        let token = defaults.value(forKey: key) as? String
        if token == nil{
            let vc = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            navigationController.pushViewController(vc, animated: true)
            
        }
        return token
        
    }
    func getBoolKey(key: String) -> Bool {
        let defaults = UserDefaults.standard
        let status = defaults.bool(forKey: key)
        return status
    }
    
    
    
    func setIntValue(value: Any, key: String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
        defaults.synchronize()
    }
    
    func getIntValue(forKey key: String) -> Int? {
        let defaults = UserDefaults.standard
        let value = defaults.value(forKey: key) as? Int
        return value
    }
    
    func getDoubleValue(forKey key: String) -> Double? {
        let defaults = UserDefaults.standard
        let value = defaults.value(forKey: key) as? Double
        return value
    }
    
    func clearValue(forKey key: String) {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: key)
    }
}

struct GlobalKey {
    static let apiToken = "api_token"
    static let regid = "regid"
    static let userType="user"
    static let customername="customername"
    static let customeremail="customeremail"
    static let customeraddress="customeraddress"
    static let phoneno="phoneno"
    static let latitude = "latitude"
    static let longitude = "longitude"
    static let baseUrl = "http://manojgas.shirantechnologies.com/api"
  
}






